var mongoose=require('mongoose');
var Broadcast=mongoose.model('EmailBroadcast');
var Subscriber=mongoose.model('EmailSubscriber');
// Load the full build.
var _ = require('lodash');
//using hook schema for setting delete children if the parent have deleted
//
const Json2csvTransform=require('json2csv').Transform;

exports.getBroadcast=function(req,res){
      
    Broadcast.find({user_id:req.body.user_id,'broadcast_status':{$ne:"delete"}}).sort({broadcast_date:-1}).exec(function(err,result){
        if (err) throw err;
        res.send(result);
    })

}
exports.getReport=function(req,res){

    let options={
        sort:     { broadcast_date: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
    let email_broadcast_id=req.body.data.email_broadcast_id;
    let status=req.body.data.status;
    var query={};
    if(email_broadcast_id!="all"){
        query.broadcast=email_broadcast_id;

    }

    if(status!="all"){
        query.status=status;
    }

    query.user_id=req.body.data.user_id;
    query.status={$ne:"delete"};
    Subscriber.paginate(query,options).then(response=>{
        res.send(response);
    }).catch(function(){
        //doing nothing if any error;
    });
}


exports.download=function(req,res){
    let email_broadcast_id=req.body.data.email_broadcast_id;
    let status=req.body.data.status;

    var query={};
    if(email_broadcast_id!="all"){
        query.broadcast=email_broadcast_id;

    }

    if(status!="all"){
        query.status=status;
    }



    // var cursor=Subscriber.find(query).exec(function(err,response){
    //     res.send(response);
    // });
    query.user_id=req.body.data.user_id;
    query.status={$ne:"delete"};
    var cursor=Subscriber.find(query).cursor({"transform":JSON.stringify});
    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename=sms-broadcastz.csv');
    res.set('Content-Type', 'text/csv');
    var dataReport={};

    var parse=function(data){
        console.log(data);
    }
    const transformOpts = { highWaterMark: 16384, encoding: 'utf-8' };
    const fields=['phone','name','status'];
    const json2csv = new Json2csvTransform({fields}, transformOpts);
   
    const processor = cursor.pipe(json2csv).pipe(res.type('blob'));
     
    // You can also listen for events on the conversion and see how the header or the lines are coming out.
    // json2csv
    //   .on('header', header => console.log(header))
    //   .on('line', line => console.log(line))
    //   .on('error', err => console.log(err));
    // cursor.pipe(res.type('json'));
    // cursor.on('data',function(data){
    //     // phone:String,
    //     // name:String,
    //     // messsage:String,
    //     // status:String,
        
    //     const fields=['phone','name','status'];
    //     const dataCsv= new json2csv({fields});
    //     let parse=dataCsv.parse(data);
    //     dataReport=parse;
    //     console.log(dataReport);

    // });


    // cursor.on('close',function(data){
    //     res.status(200).send(dataReport);
    // });
    
}