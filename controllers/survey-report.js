var mongoose=require('mongoose');
const {Pool, Client} =require('pg'); 

require('../model/Broadcast.call.model');
require('../model/broadcast.contact-list.model');
require('../model/Broadcast.contact-group.model')
var BroadcastCall=mongoose.model("BroadcastCall");
var ContactList=mongoose.model("BroadcastContactList");
var ContactGroup=mongoose.model("BroadcastGroup");
const axios=require('axios');
const moment=require('moment');

// Load the full build.
var _ = require('lodash');
//using hook schema for setting delete children if the parent have deleted
//
const Json2csvParser=require('json2csv').Parser;

exports.getCallRecords=async function(req,res){
    let parameter= req.body;
    let broadcast_call_id=req.body.data.account_code;
    let recordType=req.body.data.hangup_cause;
    var getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.findById(broadcast_call_id,function(err,res){
            resolve(res);
        })
    })

    let getData;

    try{
        getData=await axios({
            method: 'post',
            url: process.env.PHP_BACKEND+'/broadcast/report',
            data: parameter
        });

        let dataBroadcast=await getBroadcastCall;
      
      
        //mapping data
        let newData=[];
        if(getData.data.status=="success"){
           await Promise.all(getData.data.data.row.map(async (val)=>{
                //get data from contact list
                //get data broadcast
                let model={};

                //get from json data
                //let jsonData=JSON.parse(val.channel_name);
               
                let spliData=val.channel_name.split('/');
                //channel_name sofia/external/08132393434;

                //determine number if status NO_ANSWER using
                // if(val.hangup_cause=="NO_ANSWER" ){
                //     model.phone_number=val.destination_number;
                // }else{
                 model.phone_number=spliData[2];
                

                model.start_at=moment.unix(val.start_epoch).tz('Asia/Jakarta').format('D MMMM YYYY h:mm:ss A');
                model.call_duration=msToTime(val.billmsec);
                model.hangup_cause=val.hangup_cause;
                //get name of phone number;
                var ObjectId = mongoose.Types.ObjectId; 
                var phone=phoneFormat(model.phone_number);

                var cg=new ObjectId(dataBroadcast.phonebook);
           
                
                let getNumber=new Promise((resolve,reject)=>{
                    ContactList.findOne({phone:phone,contact_group:cg}).exec().then(function(res){
                        resolve(res);
                    }).catch(function(err){
                        reject(err);
                    })
                });
                let contact;
                try{
                    contact=await getNumber;
                 
                }catch(err){
                    console.log(err);
                }
                
                if(contact){
                    model.name=contact.name;
                }else{
                    model.name="unknown";
                }
                newData.push(model);
                //
            }))
        }

        getData.data.data.row=[]
        getData.data.data.row=newData;
 
        let nData={
            count_result:getData.data.data.count_results,
            row:getData.data.data.row
        }
        return res.send({success:true,data:nData});
    }catch(err){
        console.log(err);
        return res.send({success:false,data:[]});
        console.log("cannot connect to phapi backend");
    }

   

}
exports.download=async function(req,res){

    let broadcast_call_id=req.body.broadcast_call_id;
    let option_survey_id=req.body.option_survey_id;
    const pool = new Pool();
    const client=await pool.connect();

    

    var getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.findById(broadcast_call_id,function(err,res){
            resolve(res);
        })
    })
    let dataBroadcast
    try{
      dataBroadcast =await getBroadcastCall;

    }catch(err){
        console.log("cannot fetch broadcast call data");
    }

    //get report from v_ivr_survey_report by ivr_menu_option_uuid and call_broadcast_uuid;
    var newQuery="";
    newQuery+="SELECT * FROM v_ivr_survey_report";
    newQuery+=" where ivr_menu_option_uuid=$1 and call_broadcast_uuid=$2";
    newQuery+=" order by created_at asc";
    let ivrSurvey;

    let ivrMenu;
    let data=[];
    const fields=[];
    try{
        ivrSurvey =await client.query(newQuery, [option_survey_id,broadcast_call_id]);
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_menu_options";
        newQuery+=" where ivr_menu_option_uuid=$1";
        ivrMenu=await client.query(newQuery, [option_survey_id]);
        let contactGroup=await ContactGroup.findById(dataBroadcast.phonebook).exec();

        ///close pg connection to avoid memory high usage in postgre db
        await client.release();
        await pool.end();
        await contactGroup.content_desc.map((dt)=>{
            fields.push(dt);
        })
      
        await Promise.all(ivrSurvey.rows.map(async (val)=>{
          
            try{
    
                let destiNumber=phoneFormat(val.destination_number);
               // console.log(destiNumber);
                let contact=await ContactList.findOne({phone:destiNumber,contact_group:dataBroadcast.phonebook}).exec();
                if(contact){
                  
                    let dataParse=JSON.parse(JSON.stringify(contact.content_data));
                    let dataContact=await Object.keys(dataParse);
                    let dataDesc={};
                    await dataContact.map((dt)=>{
                        dataDesc[dt]=dataParse[dt];
                    });
                    data.push(dataDesc);
                }
            }catch(err){
                console.log(err);
                res.send({success:false,message:"error"});
                console.log("cannot fetch contact list");
            }
        }))
    }catch(err){
        console.log("cannot find ivr survey")
    }
    
    //get contact list by number phone contact group(get from broadcastCall);

    var getContactList="";
    

    // var cursor=Subscriber.find(query).exec(function(err,response){
    //     res.send(response);
    // });
    let nameOfFile;
    if(ivrMenu.rows.length>0){
        nameOfFile='Result_Survey_of_'+ivrMenu.rows[0].ivr_menu_option_description.replace(/[ ,.]/g,'_')+'.csv';
    }else{
        nameOfFile='Result_Survey.csv';
    }

    

    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename='+nameOfFile);
    res.set('Content-Type', 'text/csv');
    res.type('blob');
    
    const json2csvParser = new Json2csvParser({ fields });
    const csv = json2csvParser.parse(data);


    res.send(csv);
}

exports.downloadCallDetail=async function(req,res){
    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename=Call-detail-records.csv');
    res.set('Content-Type', 'text/csv');
    res.type('blob');


    let parameter= req.body;
    let broadcast_call_id=req.body.data.account_code;
    let recordType=req.body.data.hangup_cause;
    var getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.findById(broadcast_call_id,function(err,res){
            resolve(res);
        })
    })

    
    let getData;
    try{
        getData=await axios({
            method: 'post',
            url: process.env.PHP_BACKEND+'/broadcast/report',
            data: parameter
        });

        //let dataBroadcast=await getBroadcastCall;
      
        //mapping data
        let newData=[];
        if(getData.data.status=="success"){
           await Promise.all(getData.data.data.row.map((val)=>{
                //get data from contact list
                //get data broadcast
                let model={};
                //get from json data
                //let jsonData=JSON.parse(val.json);
                let spliData=val.channel_name.split('/');
                //channel_name sofia/external/08132393434;

                //determine number if status NO_ANSWER using
                // if(val.hangup_cause=="NO_ANSWER" ){
                //     model.phone_number=val.destination_number;
                // }else{
                model.phone_number=spliData[2];
    
                model.start_at=moment.unix(val.start_epoch).tz('Asia/Jakarta').format('D MMMM YYYY h:mm:ss A');
                model.call_duration=msToTime(val.billmsec);
                model.hangup_cause=val.hangup_cause;
                //get name of phone number;
                var ObjectId = mongoose.Types.ObjectId; 
                var phone=phoneFormat(model.phone_number);

                //var cg=new ObjectId(dataBroadcast.phonebook);
           
                // let getNumber=new Promise((resolve,reject)=>{
                //     ContactList.findOne({phone:phone,contact_group:cg}).exec().then(function(res){
                //         resolve(res);
                //     }).catch(function(err){
                //         reject(err);
                //     })
                // });
                // let contact;
                // try{
                //     contact=await getNumber;
                 
                // }catch(err){
                //     console.log(err);
                // }
                
                // if(contact){
                //     model.name=contact.name;
                // }else{
                //     model.name="unknown";
                // }
                newData.push(model);
                //
            }))
        }

        getData.data.data.row=[]
        getData.data.data.row=newData;
 
        let nData={
            count_result:getData.data.data.count_results,
            row:getData.data.data.row
        }
      
      
        const fields=["phone_number","start_at","call_duration","hangup_cause"];
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse(getData.data.data.row);
        
    
        return res.send(csv);
    }catch(err){
        console.log(err);
        const fields=["Phone Number","Name","Start At","Call Duration","Hangup_Cause"];
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse([]);
        return res.send(csv);
        console.log("cannot connect to phapi backend");
    }
}

function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24);
  
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
  
    return hours + ":" + minutes + ":" + seconds ;
  }
var matchPhoneFormat=function(number){
    let match= number.match(/^[0-9]*$/g);
    return match!=null?true:false;
}

var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}