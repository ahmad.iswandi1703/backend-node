const Broadcast=require('../model/smsbroadcast.model');
const Subscriber=require('../model/subscriberSms.model');

let amqp = require('amqplib/callback_api');
let amqpurl= process.env.AMQP;
exports.getSmsBroadcast=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { broadcast_date: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
    };
   
    Broadcast.paginate({'user_id':req.body.user_id,'broadcast_status':{$ne:"delete"}}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};


exports.getSmsSubscriber=function(req,res){
//-----Post request
    //data :{broadcast_id:''}
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { broadcast_date: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
//    Subscriber.find({"broadcast":req.body.data.broadcast_id}).exec(function(err,response){
//        res.send(response);
//    })
    Subscriber.paginate({"broadcast":req.body.data.broadcast_id}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
        
    });
}

exports.sendBroadcastToMessageQueue=function(broadcastData,templateId,filename){
    amqp.connect(amqpurl, function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'smsbroadcast';
          ch.assertQueue(q, {durable: true});
          // Note: on Node 6 Buffer.from(msg) should be used
          console.log(templateId);
            console.log(broadcastData);
          let data={
              broadcast_data:broadcastData,
              filename:filename,
              template_id:templateId
          }
          let string=JSON.stringify(data);
          ch.sendToQueue(q, Buffer.from(string));
          console.log(" [x] Sent 'Broadcast Message!'");
        });

        setTimeout(function() { conn.close();}, 1500);
      });
}

exports.sendManualBroadcastToMessageQueue=function(broadcastData,filename,template){
    amqp.connect(amqpurl, function(err, conn) {
        conn.createChannel(function(err, ch) {
          var q = 'smsbroadcastmanual';
          ch.assertQueue(q, {durable: true});
          // Note: on Node 6 Buffer.from(msg) should be used
          console.log(template);
            console.log(broadcastData);
          let data={
              broadcast_data:broadcastData,
              filename:filename,
              template:template
          }
          let string=JSON.stringify(data);
          ch.sendToQueue(q, Buffer.from(string));
          console.log(" [x] Sent 'Broadcast Message!'");
        });

        setTimeout(function() { conn.close();}, 1500);
      });
}

exports.deleteSmsBroadcast=function(req,res){
    let broadcast_id=req.params.broadcast_id;

    Broadcast.findById(broadcast_id).then((val)=>{
        if(val){
            val.broadcast_status="delete";

            val.save((err)=>{
                if(err){
                    return res.json({
                        status:false,
                        errors:{
                            code:"DB ERROR",
                            message:"CANNOT DELETE DATA"
                        },data:{}
                    });  

                }
                return res.json({
                    status:true,
                    errors:{

                    },data:{}
                });  
            });
        }
    }).catch(err=>{
        return res.json({
            status:false,
            errors:{
                code:"DB ERROR",
                message:"CANNOT FIND BROADCAST"
            },data:{}
        });  
    })
};