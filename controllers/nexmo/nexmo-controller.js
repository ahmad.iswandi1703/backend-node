const Subscriber=require('../../model/whatsap.broadcast.subcscriber.model');
const moment=require('moment');
exports.statusCallback=function(req,res){

    try{
        const message_uuid = req.body.message_uuid;
        const message_status = req.body.status;

    //status submitted, delivered, read
	    let dateUpd=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
        Subscriber.findOneAndUpdate({sid: message_uuid}, {$set:{status:message_status, updated_at:dateUpd}}, {returnOriginal:false}, (err, doc) => {
            if (err) {
                console.log("Something wrong when updating data!");
            }
            console.log(doc);
        });    
    }catch(err){
        res.sendStatus(200);
    }
}
exports.inBound=function(req,res){
    // const messageSid = req.body.MessageSid;
    // const messageStatus = req.body.MessageStatus;

	let dateUpd=new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Jakarta'
          });
        // Subscriber.findOneAndUpdate({sid: messageSid}, {$set:{status:messageStatus, updated_at:dateUpd}}, {returnOriginal:false}, (err, doc) => {
        //     if (err) {
        //         console.log("Something wrong when updating data!");
        //     }

        //     console.log(doc);
        // });

    res.sendStatus(200);
}