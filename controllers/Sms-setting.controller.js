const mongoose = require('mongoose');
const Setting= mongoose.model('SmsSetting');
const uuid=require('uuid');

const moment=require('moment-timezone');

let DateNow=moment().tz('Asia/Jakarta');

exports.setSetting=function(req,res,next){

    let setting=new Setting();
    setting.gateway_name=req.body.gateway_name;
    setting.gateway_url=req.body.gateway_url;
    setting.gateway_token=req.body.gateway_token;
    setting.gateway_secret=req.body.gateway_secret;
    setting.gateway_number=req.body.gateway_number;
    setting.gateway_specific_sid=req.body.gateway_specific_sid;
    setting.status=req.body.status;
    setting.type=req.body.type; /// all user(default), specific user
    setting.user_id=req.body.user_id;
    setting.created_by=req.body.created_by;
    setting.updated_at=DateNow.format();
    setting.created_at=DateNow.format();

    setting.save(function(err){
        if (err) {
            return next(err);
        }
        return res.json({ success:"true",message:setting});     
    })
               

}

exports.getSmsSetting=function(user_id){
    return new Promise(function(resolve,reject){
        //check in the CallSetting if the user_id matches and type of setting is specific user if not then get default setting
        Setting.find({},function(err,response){
            if (err){
                reject(err);
            }

            let userFind=response.filter(function(value){
                return value.user_id==user_id && value.type=="specific"
            });

            if(userFind.length>0){
                resolve(userFind[0]);
            }else{
                let defaultSetting=response.filter(function(value){
                    return value.type="all"; //this type all == default
                });
                
                if(defaultSetting.length>0){
                    resolve(defaultSetting[0]);
                }else{
                    reject("setting not found");
                }
            }
        })
    });
}

    
    
