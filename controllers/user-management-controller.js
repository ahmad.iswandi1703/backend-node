const mongoose = require('mongoose');
const passport = require('passport');
const User=mongoose.model('User');
const moment=require('moment-timezone');
let DateNow=moment().tz('Asia/Jakarta');

exports.newUser=function(req,res,next){
  const { body: { user } } = req;

  if(!user.email) {
    return res.status(422).json({
      success:false,
      errors: {
        email: 'Username is required',
      },
    });
  }

  if(!user.password) {
    return res.status(422).json({
      success:false,
      errors: {
        password: 'Password is required',
      },
    });
  }

  //cek if user has existed
  User.find({'email':user.email}).exec(function(err,resp){
      if (err) return handleError(err);

      
      if(resp.length>0){
        return res.json({
            success:false,
            errors:{
                message:'User exist'
            }
          })
      }else{
        const finalUser = new User(user);
        finalUser.user_type=user.user_type; // SUPER ROOT, ADMIN ROOT, ADMIN

        if(user.office=="null"){
          finalUser.office=null;
        }else{
          finalUser.office=user.office;
        }
        finalUser.user_privelage=null;
        finalUser.setPassword(user.password);
        finalUser.created_at=DateNow.format();
        finalUser.updated_at=DateNow.format();
        return finalUser.save()
          .then(() => res.json({success:true,message: finalUser.toAuthJSON() }));
      }

  })
  
}

exports.getAllUser=function(req,res,next){
  let options={
    sort:     { created_at:1},
    lean:     true,
    populate:"office",
    page:   req.body.offset, 
    limit:    req.body.limit
  };



  User.paginate({}, options)
  .then(response => {
    /**
     * Response looks like:
     * {
     *   docs: [...] // array of Posts
     *   total: 42   // the total number of Posts
     *   limit: 10   // the number of Posts returned per page
     *   page: 2     // the current page of Posts returned
     *   pages: 5    // the total number of pages
     * }
    */
      if(response.docs.length >0){
        res.send({success:true,message:response});
      }else{
        res.send({success:false,message:"Data not found"});
      }
     
  })
  .catch(function(err){
      //handle query error;
      // set loggin

      return next(err);
  });
}

exports.changePassword=function(req,res,next){
  const { body: { user } } = req;

  User.findById(user.id).exec(function(err,resp){
    if (err) return handleError(err);

      resp.user_type=user.user_type; // SUPER ROOT, ADMIN ROOT, ADMIN
      if(user.office=="null"){
        resp.office=null;
      }else{
        resp.office=user.office;
      }
      resp.email=user.email;
      resp.user_privelage=null;
      resp.updated_at=DateNow.format();
      resp.setPassword(user.password);
      return resp.save()
        .then(() => res.json({success:true,message: "change password success" }));

  })
}

exports.deleteUser=function(req,res,next){
  //check if broadcast (call,whatsapp,sms) exist
  let id=req.params.uid;
  User.findByIdAndRemove(id,function(err,media){
    if (err)
        return next(err);
    res.send({ success: true, message: "Deleted" });
  });
}



exports.getUser=function(req,resp,next){
  try{
    User.find({},function(err,res){
      if(err) return next(err);

      if(res.length>0){
        return resp.send({ success: true, message: res });
      }else{
        return resp.send({ success: false, message: "Cannot Find User" });
      }
    })
  }catch(err){
    return next(err);
  }
}


exports.login=function(req,res,next){
    const { body: { user } } = req;

    if(!user.email) {
      return res.status(422).json({
        errors: {
          email: 'is required',
        },
      });
    }
  
    if(!user.password) {
      return res.status(422).json({
        errors: {
          password: 'is required',
        },
      });
    }
  
    return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
      if(err) {
        return next(err);
      }
  
      if(passportUser) {
        const user = passportUser;
        // user.token = passportUser.generateJWT();
  
        return res.json({ user: user.toAuthJSON() });
      }
  
      return status(400).json({
          errors: {
            message: 'login failed',
            code:'LGN_FAILED'
          },
      });
    })(req, res, next);
}

exports.current=function(req,res,next){

    return User.findById(req.body.payload.id)
      .then((user) => {
        if(!user) {
          return res.json({success:"false",message:"cannot find data"});
        }
  
        return res.json({success:true,message:user});
      });
}