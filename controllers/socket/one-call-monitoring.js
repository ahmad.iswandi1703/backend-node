var mongoose=require('mongoose');
const {Pool, Client} =require('pg'); 

require('../../model/Broadcast.call.model');
require('../../model/broadcast.contact-list.model');
require('../../model/Broadcast.call.subscriber.model');
require('../../model/Ocallmon.model');

var BroadcastCall=mongoose.model("BroadcastCall");
var ContactList=mongoose.model("BroadcastContactList");
var BroadcastSubscriber=mongoose.model("BroadcastSubscriber");
var ObjectId = require('mongoose').Types.ObjectId; 
var Setting=mongoose.model('OcallMonSetting');
var csv=require('csvtojson');
var parseData=require('../../lib/parse-number');

var OcalMon=mongoose.model("OCallMon");

var moment=require('moment-timezone');

const uuid=require('uuid');
const SettingController=require('../../controllers/setting-controller');

exports= module.exports=async function(io){
    //save user id
    //cek if there is one call and monitoring 
    
    io.sockets.on("connection",function(socket){

        //{
        // contact.phone:""
        // contact.name:"",
        // contact.messageError:"",
        // contact.status:"",
        // contact.enabled:"",    
        // contact.content_data:"",
        // contact.user_id:""}
 
        //get setting data;
        socket.on('first_handshake',async function(data){
            console.log(data);
            let user_id=data.user_id;
            let returnData={
                status:"", //ready, lock
                data:""
            };

            //get data ocallmon if (exist) send the data ocallmon
            let lastOcall;
            try{
                lastOcall=await getLastOcall(user_id);
                if(isEmpty(lastOcall)){
                    returnData.status="ready";
                }else{
                    returnData.status="lock";
                }
                returnData.data=lastOcall;
                socket.emit('first_handshake_approve',{ status:"true",errors:{},data:returnData});
            
            }catch(err){
                console.log(err);
                returnData.status="ready";
                lastOcall={};
                returnData.data=lastOcall;
                socket.emit('first_handshake_approve',{ status:"false",errors:{},data:returnData});
            }

            //emit to socket that receive data
        });

        socket.on('call_status',async function(data){
            //get status of call in queue;
       
            let broadcast_id=data.account_code;
            let subscriber;

            try{
                subscriber=await getBroadcastSubscriber(broadcast_id);
                socket.emit('call_status',{ status:"true",errors:{},data:subscriber});
            }catch(err){
                socket.emit('call_status',{ status:"false",errors:{},data:null});
            }
            
        })

        socket.on('survey_result',async (data)=>{
            let user_id=data.user_id;
            let account_code=data.account_code;
            console.log(account_code);
            const pool = new Pool();
            const client=await pool.connect();
            
            let setting;
            try{
                setting=await getSetting(user_id,'active');

                let ivrMenuOptionUid="";

                let count=1;
                setting.ivr_menu_options.filter((val)=>{
                    ivrMenuOptionUid+="'"+val+"'";
                    if(setting.ivr_menu_options.length!=count){
                        ivrMenuOptionUid+=",";
                    }

                    count++;
                })
                let newQuery="";
                newQuery+="SELECT * FROM v_ivr_survey_report";
                newQuery+=" where call_broadcast_uuid='"+account_code+"'";
                newQuery+=" and ivr_menu_option_uuid in ("+ivrMenuOptionUid+")"
                newQuery+=" order by created_at asc";         
                let ivrSurvey =await client.query(newQuery);


                newQuery="";
                newQuery+="SELECT * FROM v_ivr_menu_options";
                newQuery+=" where ivr_menu_option_uuid in ("+ivrMenuOptionUid+")"
                let getMenuName=await client.query(newQuery);

                let newData={
                    report:ivrSurvey.rows,
                    menu:getMenuName.rows
                }

                
                socket.emit('survey_result',{ status:"true",errors:{},data:newData});
               
            }catch(err){
                console.log(err);
                socket.emit('survey_result',{ status:"false",errors:{},data:null});
            }

            await client.release();
            await pool.end();
        })

        socket.on('get_channels_active',async function(data){

        })

        // socket.on('checking_number',async function(data){
        //     contact_number=[];
        //     let returnData={
        //         error_number:0,
        //         success_number:0,
        //     }
        //     //get data text 
        //     let contact=data.contact;
        //     campaign_name=data.campaign_name;
        //     let colDesc=data.colDesc;

        //     if(colDesc.length==0){
        //         colDesc.push("Nomor");
        //     }

        //     if(user_id===""){
        //         socket.emit("reject",{message_error:"cannot find user id"});
        //     }else{
        //         const converter=csv({
        //             noheader:true,
        //             trim:false,
        //             delimiter:";",
        //             quote:"off"
        //         });
    
        //         converter.fromString(contact).on("data",async function(data){
        //             const jsonStr= data.toString('utf8')
        //             const json=JSON.parse(jsonStr);
        //             let contact={};
        //             try{
        //                 parseData(json,"field",colDesc,async (err,returnData)=>{
        //                     if(err){
        //                         ///count error number
        //                         returnData.error_number+=1;
        //                     }else{
        //                         returnData.success_number+=1;
        //                         //saving data to database
        //                         contact.phone=returnData.phone;
        //                         contact.name=returnData.name;
        //                         contact.messageError=returnData.message_error;
        //                         contact.status="check";
        //                         contact.enabled=returnData.enabled;    
        //                         contact.content_data=returnData.content_data;
        //                         contact.user_id=user_id;
        //                         contact_number.push(contact);
        //                     }
        //                 })
        //             }catch(err){
        //                 console.log(err);
        //             }
        //         });
        //         //parse contact
        //         //save data to broadcast subscriber
        //         socket.emit('number_approve',returnData);
        //     }


        // });

        // socket.on('submit-data',async function(data){
        //     //save data broadcast subscriber
        //     //save data

        //     //get default application from setting
            
        //     if(user_id==="" || campaign_name==="" || contact_number.length==0){
        //         socket.emit("reject",{message_error:"cannot handle this request"});
        //     }else{
        //         //
        //         makeCampaign(campaign_name,DEFAULT_APPLICATION,user_id,socket);
        //     }
        // })



    });
    //activity
    // 
}

//get las activity ocallmon
function getLastOcall(user_id){
    return new Promise(function(resolve,reject){
        OcalMon.findOne({call_status:"running",user_id:user_id}).sort({created_at:-1}).then(function(val){
            resolve(val);
        }).catch(err=>{
            reject(err);
        });
    });
}

function getBroadcastSubscriber(broadcast_id){
    return new Promise(function(resolve,reject){
        BroadcastSubscriber.find({broadcast:broadcast_id}).sort({created_at:1}).then(function(val){
            resolve(val);
        }).catch(err=>{
            reject(err);
        });
    });
}

function makeCampaign(campaign_name,application,user_id,socket){
    SettingController.getCallSetting(req.body.user_id).then(async function(val){
        // if (err){
        //     next(err);
        // }
        if(!val){
            socket.on("reject",{messageError:"cannot find call setting"});
        }
        //set data request to Database
        let call=new OcalMon();
        call.uid=uuid.v4();
        call.presence_id=call.uid;
        call.campaign_name=campaign_name; //this campaign id
        call.application=application;
        call.account_code=call.uid;
        call.call_setting=val._id;

        //Preparation, Starting , Proggress, Finish, Error
        call.call_status="running";
        call.user_id=user_id;
        var nDate = moment().tz('Asia/Jakarta'); 
        call.created_at=nDate.format();
        call.updated_at=nDate.format();
        //get concurent limit from setting
        try{
            await call.save(async function(err){
                if(err) {throw(err)};
                //save data number to broadcast;
                let successInsert=0;
                let errorInsert=0;
                await Promise.all(contact.map(async function(val){
                    //save data to db
                    try{
                        let insertSubcriber=await insertSubscriber({phone:val.phone},val._id);
                        successInsert+=1;
                    }catch(err){
                        errorInsert+=1;
                        console.log(err);
                    }
                    
                }))
                socket.on("success_submit",{message:"success make campaign",success:successInsert,error:errorInsert});

            })
        }catch(err){
            socket.on("reject",{messageError:"error to make campaign"});
        }
    }).catch((err)=>{
        //send request error data setting
        socket.on("reject",{messageError:"call setting error "});
    })

}

var insertSubscriber=function(subscriberData,setting){
    let dateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
    });
    return new Promise(function(resolve,reject){
        var subscriber=new Subscriber();
        subscriber.phone=subscriberData.phone;
        subscriber.created_at=dateNow
        subscriber.updated_at=dateNow
        subscriber.broadcast=setting;
        subscriber.tryCall=0;
        subscriber.status='in_queue';//(in_queue, in_call, no_answer, answer,re_call, error, fail, finish)
        subscriber.save(function(err){
            if(err){
                console.log(err);
            }
            resolve("success");
        })
    }) 
 }


 function getSetting(user_id,status){
    return new Promise((resolve,reject)=>{
        Setting.findOne({user_id:user_id,status:status}).sort({created_at:-1}).exec(function(err,setting){
            if (err) {
                reject(err);
            }
            
            resolve(setting)   
        })

    })
}

function isEmpty(data){
    for(var key in data) {
        if(data.hasOwnProperty(key))
            return false;
    }
    return true;
  
}

//check active ocallmon


//stop ocallmon


//


//create call


//submit string


