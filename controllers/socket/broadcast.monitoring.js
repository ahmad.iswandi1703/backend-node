var mongoose=require('mongoose');
const {Pool, Client} =require('pg'); 

require('../../model/Broadcast.call.model');
require('../../model/broadcast.contact-list.model');
require('../../model/Broadcast.call.subscriber.model');
var BroadcastCall=mongoose.model("BroadcastCall");
var ContactList=mongoose.model("BroadcastContactList");
var BroadcastSubscriber=mongoose.model("BroadcastSubscriber");
var ObjectId = require('mongoose').Types.ObjectId; 

var moment=require('moment-timezone');

exports= module.exports=async function(io){
    io.sockets.on("connection",async function(socket){
        //socket for update answers call, no answers call, remains call
        var broadcastCall={};
        var accountcode=""; //account code is same as broadcast_call_uuid;
        var user_id="";

        
        await socket.on("connection_monitoring",async function(data){
            //data will filled with broadcast id or call broadcast id
            //console.log(data);
            broadcastCall=data.broadcast_id;
            var getBroadcastCall=new Promise(function(resolve,reject){
                BroadcastCall.findById(broadcastCall,function(err,res){
                    resolve(res);
                })
            })
            //get data broadcass
            try{
                let dataBroadcast=await getBroadcastCall;
                broadcastCall=dataBroadcast;
            }catch(err){
                console.log("cannot find broadcast call data");
            }

            socket.emit("monitoring_approve",{approve:true, data:broadcastCall});
       
            accountcode=data.broadcast_id;
            user_id=data.user_id
        })

        await socket.on('update_call_broadcast',async function(){
            var getBroadcastCall=new Promise(function(resolve,reject){
                BroadcastCall.findById(broadcastCall._id,function(err,res){
                    resolve(res);
                })
            })
            //get data broadcass
            try{
                let dataBroadcast=await getBroadcastCall;
                broadcastCall=dataBroadcast;
            }catch(err){
                console.log("cannot find broadcast call data");
            }

            socket.emit("receive_update",{data:broadcastCall});
        })

        await socket.on("on_detail_records",async function(){
            const pool = new Pool();
            const client=await pool.connect();
            //data model to be send
            let data=
            {
                call_answer:{
                    status_data:false,
                    message:"",
                    data:0,
                },
                no_answer:{
                    status_data:false,
                    message:"",
                    data:0
                },
                remain_call:{
                    status_data:false,
                    message:"",
                    data:0,
                }
            };

         
            //fetch data from db xml cdr table
            //get data from cdr where call hangup == answer'
            //Hangup cause

            //NORMA_CLEARING
            //NORMAL_CIRCUIT_CONGESTION
            //NO_ANSWER
            //NO_USER_RESPONSE
            //DESTINATION_OUT_OF_ORDER
            //ORIGINATOR_CANCEL
            //USER_NOT_REGISTERED
            try{
                let query="";

                query+="SELECT COUNT(*) FROM v_xml_cdr";
                query+=" where accountcode=$1";
                query+=" and hangup_cause in ('NORMAL_CLEARING','NORMAL_CIRCUIT_CONGESTION')";
            
                const dataAnswer=await client.query(query, [accountcode]);
                data.remain_call.status_data=true;
                data.call_answer.message="oke";
                data.call_answer.data=dataAnswer.rows[0].count;
            }catch(err){
                console.log(err);
                console.log("cannot fetch data call answers2");
                data.call_answer.message="failed to fetch data";
                data.call_answer.data=0;
            }

          

            //get data from cdr where call hangup == no answer
            try{
                var newQuery="";
                newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
                newQuery+=" where accountcode=$1";
                newQuery+=" and hangup_cause not in  ('NORMAL_CLEARING','NORMAL_CIRCUIT_CONGESTION')";
              //  newQuery+=" and hangup_cause  in ('NO_ANSWER','NO_USER_RESPONSE','CALL_REJECTED','DESTINATION_OUT_OF_ORDER','ORIGINATOR_CANCEL','USER_NOT_REGISTERED')";
                const dataNoAnswer=await client.query(newQuery, [accountcode]);
              

                data.no_answer.status_data=true;
                data.no_answer.message="oke";
                data.no_answer.data=dataNoAnswer.rows[0].count;
               
               

            }catch(err){
                console.log("cannot fetch data call answers3");
                data.no_answer.message="failed to fetch data";
                data.no_answer.data=0;
            }

             //to get data of remains call substract sum of cdr with contact list
             try{
                var newQuery="";
                newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
                newQuery+=" where accountcode=$1";
                const allCalls=await client.query(newQuery, [accountcode]);
             //change bandingkan dengan call broadcast

                const contactList=await  ContactList.countDocuments({contact_group:broadcastCall.phonebook}).exec();
                //get sum of subscriber
                const countSubscriber=await BroadcastSubscriber.countDocuments({broadcast:broadcastCall._id,status:{$ne:'in_queue'}}).exec();
                data.remain_call.status_data=true;
                data.remain_call.message="oke";
                //change code below 
                //data.remain_call.data=Math.ceil(contactList-allCalls.rows[0].count);
                data.remain_call.data=Math.ceil(contactList-countSubscriber);
                //console.log(contactList-allCalls.rows[0].count);
            }catch(err){
                console.log(err);
                console.log("cannot fetch data call answers4");
                data.remain_call.message="failed to fetch data";
                data.remain_call.data=0;
            }

            await client.release();
            await pool.end();
        

            //emit data to 
            socket.emit("receive_detail_records",data);

        })

        await socket.on("on_active_call",async function(){
            var fsServer = process.env.FS_SERVER,
            fsPass   = process.env.FS_PASSWORD,
            fsPort   = process.env.FS_PORT;
        
            FS = require('esl');
            var fs_command = function(cmd) {
              var client = FS.client(function(){
                this.api(cmd)
                .then( function(res) {
                  // res contains the headers and body of FreeSwitch's response.
                  //console.log(res);
                  let body=JSON.parse(res.body);
                  if(body.hasOwnProperty('rows')){
                      let newData=[];
                      body.rows.map((val)=>{
                        let data={};
                        data.callstate=val.callstate;
                        var nDate = moment(val.created).tz('Asia/Jakarta'); 

                        data.created=nDate.format();
                        //get number
                        let newNumber=val.name.split('/');

                        data.name=newNumber[2];
                        newData.push(data);
                      });
                    socket.emit("receive_active_call",{row_count:body.row_count,rows:newData});
                  }else{
                    socket.emit("receive_active_call",{row_count:body.row_count,rows:[]});
                    
                  }
                  //res.body.should.match(/\+OK/);
                  return null;
                })
                .then( function(){
                  this.exit();
                  return null
                 
                })
                .then( function(){
                  client.end();
                  return null;
                })
              });
              client.connect(8021,fsServer);
            
            };
            

            fs_command("show channels like "+accountcode+" as json");
      
        })

        await socket.on("survey",async function(){
            const pool = new Pool();
            const client=await pool.connect();
            //data model to be send
            //get data from cdr where call hangup == no answer
            let dataPush=[];
            try{
                var newQuery="";
                newQuery+="SELECT * FROM v_ivr_survey_pivot";
                newQuery+=" where uuid=$1";
                newQuery+=" order by created_at asc";
         
                const ivrSurvey =await client.query(newQuery, [broadcastCall.application]);

            

               // console.log(ivrSurvey.rows);
    
                await Promise.all(ivrSurvey.rows.map(async function(val){
                    try{
            
                       
                        var newQuery="";
                        newQuery+="SELECT * FROM v_ivr_menus";
                        newQuery+=" where ivr_menu_uuid=$1";
                        newQuery+=" and ivr_type='MULTI_CHOICE'";
                        let ivrMenu=await client.query(newQuery, [val.ivr_menu_uuid]);
                        
                        if(ivrMenu.hasOwnProperty('rows')){
                            await Promise.all(ivrMenu.rows.map(async function(data){
                                let dataObject={menu_name:'',menu_id:'',survey:[]};
                                dataObject.menu_name=data.ivr_menu_description;
                                dataObject.menu_id=data.ivr_menu_uuid
                                var query="";
                                //join with 
                                query+="SELECT v_ivr_menu_options.*,coalesce(options.jml,0) as survey_count  FROM v_ivr_menu_options";
                                query+=" left join (select ivr_menu_option_uuid, count(*) as jml ";
                                query+="from v_ivr_survey_report where call_broadcast_uuid='"+accountcode+"' group by ivr_menu_option_uuid)";
                                query+=" options on CAST(options.ivr_menu_option_uuid as uuid)=v_ivr_menu_options.ivr_menu_option_uuid";
                                query+=" where v_ivr_menu_options.ivr_menu_uuid=$1";
                                try{
                                    let ivrOptions=await client.query(query,[data.ivr_menu_uuid]);
                                    if(ivrOptions.hasOwnProperty('rows')){
                                        dataObject.survey=ivrOptions.rows;
                                    }
                                }catch(err){
                                    console.log("error here");
                                    console.log(err);
                                }

                                dataPush.push(dataObject);
                            }))
                        }

                    }catch(err){
                        console.log(err);
                    }
                }))

                


            }catch(err){
                console.log(err);
                console.log("cannot fetch data call answers3");

            }


            
            socket.emit('receive_survey', dataPush);

            await client.release();
            await pool.end();
        })

        await socket.on('on_queue_detail',async function(){
            
            let getSubscriber=new Promise(function(resolve,reject){
                BroadcastSubscriber.find({broadcast:ObjectId(accountcode),status:'in_queue'}).then(function(val){
                    resolve(val);
                }).catch(err=>{
                    reject(err);
                })
            });
            let subscriber;
            try{
                subscriber=await getSubscriber;
                if (subscriber.length==0){
                    subscriber=[];
                }
            }catch(err){
                subscriber=[];
            }
            socket.emit("receive_queue_detail",subscriber);
           
        })
    })
}




// async function cobaQuery(){
//     const pool = new Pool({
//         user: 'fusionpbx',
//         host: '178.128.215.77',
//         database: 'fusionpbx',
//         password: 'JizNhO4mCQHG7v6OnHc9WZaiYpw',
//         port: 5432,
//       });


//     const client=await pool.connect();
    
//     try{
//         let query="";
    
//         query+="SELECT COUNT(*) FROM v_xml_cdr";
//         query+=" where accountcode=$1";
//         query+=" and hangup_cause in ('NORMAL_CLEARING','NORMAL_CIRCUIT_CONGESTION')";
    
//         const dataAnswer=await client.query(query, ["5c4e7f685f87cb2d30593c0d"]);

//         console.log(dataAnswer.rows[0].count);

//             //NO_ANSWER
//             //NO_USER_RESPONSE
//             //DESTINATION_OUT_OF_ORDER
//             //ORIGINATOR_CANCEL
//             //USER_NOT_REGISTERED
        
//         var newQuery="";
//         newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
//         newQuery+=" where accountcode=$1";
//         newQuery+=" and hangup_cause in ('NO_ANSWER','NO_USER_RESPONSE','CALL_REJECTED','DESTINATION_OUT_OF_ORDER','ORIGINATOR_CANCEL','USER_NOT_REGISTERED')";
//         const dataNoAnswer=await client.query(newQuery, ["5c4e7f685f87cb2d30593c0d"]);
//         console.log(dataNoAnswer.rows[0].count);
//     }catch(err){
//         console.log(err);
//         console.log("cannot fetch data call answers")
//     }finally{
//         client.release()
//     }
// }

// cobaQuery();

