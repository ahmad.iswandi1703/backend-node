
var fs = require('fs');
var path = require('path');
const uuid=require('uuid');
var Broadcast=require('../../model/smsbroadcast.model');
var moment = require('moment-timezone');

let EmailController=require('../../controllers/email/email-controller');

exports=module.exports=async function(io){
    io.sockets.on("connection",async function(socket){
        let Files={};
        socket.on('StartEmailTemplate', function (data) { //data contains the variables that we passed through in the html file
            let Name = data['Name'];
            //get data template by template ID
            console.log(appRoot+"/temp");
            Files[Name] = {  
                FileSize : data['Size'],
                Data   : "",
                Downloaded : 0
            }
            let Place = 0;
            try{
                var Stat = fs.statSync(appRoot+'/temp/' +  Name);
                if(Stat.isFile())
                {
                    Files[Name]['Downloaded'] = Stat.size;
                    Place = Stat.size / 524288;
                }
            }
            catch(er){} //It's a New File
            fs.open(appRoot+"/temp/" + Name, "a", 0755, function(err, fd){
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    var extName=path.extname(appRoot+"/temp/"+Name);            
                    if(extName==".csv" || extName==".txt"){
                        //checking format            
                        dataBroadcast=data['Data'];
                        socket.emit('passessEmailTemplate',{status:true,error:{
                        },message:{}});
                        Files[Name]['Handler'] = fd; //We store the file handler so we can write to it later
                        socket.emit('MoreDataTemplate', { 'Place' : Place, Percent : 0 });
                    }else{
                        socket.emit('passessTemplate',{status:false,error:{
                            code:"FILE_ERROR"
                        },message:{}});
                    }
                }
            });
    
            // detect.fromFile('./Temp/'+Name, function(err, result) {
    
            //     if (err) {
            //       return console.log(err);
            //     }
            //     console.log(result); // { ext: 'jpg', mime: 'image/jpeg' }
            //     if(result.ext=='csv'){
            //         socket.emit('passess',true);
            //     }else if(result.ext=='txt'){
            //         socket.emit('passess',true);
            //     }else{
            //         socket.emit('passess',false);
            //     }
            // });
    
    
        });
    
        socket.on('UploadEmailTemplate', function (data){
            let dataBroadcast=data["AdditionalData"];
            let Name = data['Name'];
            Files[Name]['Downloaded'] += data['Data'].length;
            Files[Name]['Data'] += data['Data'];
            if(Files[Name]['Downloaded'] == Files[Name]['FileSize']) //If File is Fully Uploaded
            {
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    //Get Thumbnail Here
                    var extName=path.extname(appRoot+"/temp/"+Name);          
                    var newName=uuid.v4()+extName;
                    var inp = fs.createReadStream(appRoot+"/temp/" + Name);
                    var out = fs.createWriteStream(appRoot+"/contact/" + newName);
                    inp.pipe(out);
                    inp.on("end",function(){
                        fs.unlinkSync(appRoot+"/temp/" + Name, function () { //This Deletes The Temporary File
                            //Moving File Completed
                            console.log("work");
                        });
                        // exec("ffmpeg -i Video/" + Name  + " -ss 01:30 -r 1 -an -vframes 1 -f mjpeg Video/" + Name  + ".jpg", function(err){
                        //     socket.emit('Done', {'Image' : 'Video/' + Name + '.jpg'});
                        // });
    
    
                        //checking if the data doesnt have proper format
                        if (dataBroadcast.broadcastName==""){
                            dataBroadcast.broadcastName="Broadcast "+Date.now();
                        }
                        // var nDate = new Date().toLocaleString('en-US', {
                        //     timeZone: 'Asia/Jakarta'
                        // });
                        EmailController.makeBroadcast(dataBroadcast).then(async (dataB)=>{
                            //save data attachments file
                           
                            if(dataBroadcast.attachments.length>0){
                                await EmailController.makeAttachmentsBroadcast(dataB._id,dataBroadcast.attachments,dataBroadcast.user_id).then(val=>{
                                    console.log("success make email Attachments")
                                }).catch(err=>{
                                    console.log(err);
                                })
                            }
        
                            let fileName=newName;


                            //check if template_id is existed
                     
                            let template={
                                template_id:"default",
                                message:"",
                                variable:""
                            }
                            console.log(dataBroadcast);
                            if(dataBroadcast.template=='default' || dataBroadcast.template==undefined){
                                let messageBody=dataBroadcast.message;
                                template.message=messageBody;
                                template.variable=dataBroadcast.contentDesc;
                            }else{
                        
                                template.template_id=dataBroadcast.template;
                            }

                            console.log(dataB);
                            EmailController.sendManualBroadcastToMessageQueue(dataB,fileName,template);
                            socket.emit('DoneEmailTemplate', {'data' : 'Succes'});
                        }).catch(err=>{
                            console.log(err);
                            socket.emit('DoneEmailTemplate', {'data' : 'failed'});
                        })
                    })
    
                });
            }
            else if(Files[Name]['Data'].length > 10485760){ //If the Data Buffer reaches 10MB
                fs.write(Files[Name]['Handler'], Files[Name]['Data'], null, 'Binary', function(err, Writen){
                    Files[Name]['Data'] = ""; //Reset The Buffer
                    var Place = Files[Name]['Downloaded'] / 524288;
                    var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                    socket.emit('MoreDataEmailTemplate', { 'Place' : Place, 'Percent' :  Percent});
                });
            }
            else
            {
                var Place = Files[Name]['Downloaded'] / 524288;
                var Percent = (Files[Name]['Downloaded'] / Files[Name]['FileSize']) * 100;
                socket.emit('MoreDataEmailTemplate', { 'Place' : Place, 'Percent' :  Percent});
            }
        });
    })
    

}