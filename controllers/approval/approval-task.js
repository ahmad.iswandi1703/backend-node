const mongoose = require('mongoose');
const UserSchema= mongoose.model('ApprovalUser');
const Schema=mongoose.model('ApprovalSchema');
const Request=mongoose.model('ApprovalRequest');
const Task=mongoose.model('ApprovalTask');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

let engine=require('./approval-engine');

const moment=require('moment-timezone');

exports.approving=function(req,res,next){
    let task_id=req.params.task_id;
    let status=req.params.status; //approved, rejected;

    engine.action_task(task_id,status).then(val=>{
        console.log(val);
        if(val.status){
            return res.json({
                status:true,
            })
        }else{
            return res.json({
                status:false,
            })
        }
    }).catch(err=>{
        console.log(err);
        return next(err);
    })
}

exports.requestPaging=function(req,res,next){
    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        lean:true,
        populate:[{path:"approval_request",populate:[{path:'approval',model:'ApprovalSchema'},{path:'user',model:'User'}]},{
            path:"user"
        }],
        limit:    req.body.limit
    };
    var query={};
    query.user=req.body.data.user_id;
    query.status_task="request";
    Task.paginate(query,options).then(response=>{
        if(response.docs.length >0){
            res.send({status:true,data:response});
        }else{
            res.send({status:false,data:"Data not found"});
        }
    }).catch(function(err){
        //doing nothing if any error;
        return next(err);
    });

}



