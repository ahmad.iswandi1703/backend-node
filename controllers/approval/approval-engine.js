//this controllers aimed to do all jobs that related with approval proccess like insert task when request made and others
const mongoose = require('mongoose');
const UserSchema= mongoose.model('ApprovalUser');
const Schema=mongoose.model('ApprovalSchema');
const Request=mongoose.model('ApprovalRequest');
const Task=mongoose.model('ApprovalTask');
const ObjectId=mongoose.Types.ObjectId;
const uuid=require('uuid');

const moment=require('moment-timezone');

//check if 

module.exports={
    add_task:async function(request_id,approval){
        //add task 

        let returnData={
            status:false,
            data:""
        }
        //get approval scheme and then determine task active in task collection
        return new Promise(async (resolve,reject)=>{
            try{
                let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
                let scheme=await getApprovalSchema(approval);
    
                let approvalUser=await getUserApprover(approval);
    
                //determine level basic of approvalUser
                //get sort approval user by level (1 tooo -----)
                let pembanding=0;
                let userLevelBasic;
                approvalUser.map(val=>{
                  
                    if(val.level ==1){
                        userLevelBasic=val;
                    }
                  
                })
  
    
                //add to task collection with one
                let task=new Task();
                task.approval_request=request_id;
                task.user=userLevelBasic.user;
                task.status_task="request";
                task.created_at=nDate
                task.updated_at=nDate

                await task.save((err)=>{
               
                    if(err){
                        console.log(err);
                   
                        returnData.status=false;
                        returnData.data=null;
                    }else{
                     
                        returnData.status=true;
                        returnData.data=task;
                    }
                    resolve(returnData)

                })
    
            }catch(err){
                console.log(err);
                returnData.status=false;
                returnData.data=null
                resolve(returnData)
               
            }
           
        })

    },
    get_task:async function(user_id){
        let returnData={
            status:false,
            data:""
        }
        return new Promise((resolve,reject)=>{
            //get task that has status request
            Task.find({user:user_id,status:'request'}).then((val)=>{
                if(val.length>0){
                    returnData.status=true;
                    returnData.data=val;
                }
            }).catch(err=>{
                console.log(err);
            })

            resolve(returnData);
        })

    },
    action_task:async function(task_id,status){
        let nDate=moment(new Date(),'YYYY-MM-DD HH:mm','Asia/Jakarta').format();
        //action to reject or approve the task
        let returnData={
            status:false,
            errors:{
                message:"",
                code:""
            },
            data:""
        }
        // --- cek apakah user yang paling tinggi (jika ya di approve update request status);
        // --- jika approve maka cek terlebih dahulu apakah ada level berikutnya, jika tidak maka update request status
        // --- jika reject maka langsung update status request menjadi rejected.
        // --- jika approve maka edit data task dengan user berikutnya dan ubah status menjadi request
       return new Promise((resolve,reject)=>{
            getTask(task_id).then(async val=>{
                let aproval_schema_id=val.approval_request.approval;
                let approval_request_id=val.approval_request._id;

                try{
                    let approver=await getUserApprover(aproval_schema_id);


                    //get approver that has same as task user id
               
                    let userExisting=null;
                    for(let i=0; i<approver.length; i++){
                        let userTask=String(val.user);
                        let userApprover=String(approver[i].user);
                        if(userApprover===userTask){
        
                            userExisting=approver[i];
                        }
                    }
             
                    let pembanding=userExisting.level;
           
                    let userLevelNext=null;
                    //getNextLevel;
                    
                    for(let i=0; i<approver.length; i++){
                       
                        if(approver[i].user!=val.user && approver[i].level>pembanding){
                            if(approver[i].level>pembanding){
                                userLevelNext=approver[i];
                                break;
                            }
                            pembanding=approver[i].level;
                        }
                    }

                    console.log(userLevelNext);
                    if(status=="approved"){
                        
                        if(userLevelNext!=null){
                            console.log("mlebu next level");
                            //update task to the next level 
                            await Task.findById(task_id).then(async (valTask)=>{
                                valTask.user=userLevelNext.user;

                                valTask.updated_at=nDate;
                                valTask.created_at=nDate;
                                valTask.save((err)=>{
                                    if(err){
                                        returnData.status=false;
                                        returnData.data=null;
                                        returnData.errors.code="ERROR IN DB";
                                        returnData.errors.message="CANNOT ASSIGN TO NEXT USER LEVEL TASK";
                                    }else{
                                        returnData.status=true;
                                        returnData.data=valTask;
                                    }

                                   return resolve(returnData);
                
                                })
                            }).catch(err=>{
                                console.log(err,"-1");
                                returnData.status=false;
                                returnData.errors.code="ERROR IN DB";
                                returnData.errors.message="Cannot find task"
                                return resolve(returnData);
                            })        
    
                        }else{
                            //update request task  and then update to approval request 
                            await Task.findById(task_id).then(async(valTask)=>{
                                valTask.status_task="approved";
                                valTask.updated_at=nDate;
                                valTask.created_at=nDate;
                                await valTask.save(async (err)=>{
                                    if(err){
                                        returnData.status=false;
                                        returnData.data=null;
                                        returnData.errors.code="ERROR IN DB";
                                        returnData.errors.message="CANNOT SAVE NEW TASK";
                                    }else{
                                        returnData.status=true;
                                        returnData.data=valTask;

                                        await updateApprovalRequest(approval_request_id,"approved").then(val=>{
                                            if(val=="success"){
                                                console.log("success");
                                            }else{
                                                returnData.status=false;
                                                returnData.data=null;
                                                returnData.errors.code="ERROR IN DB";
                                                returnData.errors.message="CANNOT UPDATE APPROVAL REQUEST STATUS";
                                            }

                                        }).catch(err=>{
                                            console.log(err,"0");
                                            returnData.status=false;
                                            returnData.data=null;
                                            returnData.errors.code="ERROR IN DB";
                                            returnData.errors.message="CANNOT UPDATE APPROVAL REQUEST STATUS";
                                        })
                                    }
                                    return resolve(returnData);
                                })
                            }).catch(err=>{
                                console.log(err,"1");
                                returnData.status=false;
                                returnData.errors.code="ERROR IN DB";
                                returnData.errors.message="Cannot find task"
                                return resolve(returnData);
                            })  
                        }
                    }else{
                        //rejected and langsung update approval requestnya
                        //update approval request.
                        await Task.findById(task_id).then(async(valTask)=>{
                            valTask.status_task="rejected";
                            valTask.updated_at=nDate;
                            valTask.created_at=nDate;
                            await valTask.save(async (err)=>{
                                if(err){
                                    returnData.status=false;
                                    returnData.data=null;
                                    returnData.errors.code="ERROR IN DB";
                                    returnData.errors.message="CANNOT SAVE NEW TASK";
                                }else{
                                    returnData.status=true;
                                    returnData.data=valTask;

                                    await updateApprovalRequest(approval_request_id,"rejected").then(val=>{
                                        if(val=="success"){
                                            console.log("success");
                                        }else{
                                            returnData.status=false;
                                            returnData.data=null;
                                            returnData.errors.code="ERROR IN DB";
                                            returnData.errors.message="CANNOT UPDATE APPROVAL REQUEST STATUS";
                                        }
                                    }).catch(err=>{
                                        console.log(err,"2");
                                        returnData.status=false;
                                            returnData.data=null;
                                            returnData.errors.code="ERROR IN DB";
                                            returnData.errors.message="CANNOT UPDATE APPROVAL REQUEST STATUS";
                                    })
                                    
                                }
                                return resolve(returnData);
            
                            })
                        }).catch(err=>{
                            console.log(err,"3");
                            returnData.status=false;
                            returnData.errors.code="ERROR IN DB";
                            returnData.errors.message="Cannot find task"
                            return resolve(returnData);
                        }) 
                    }
                }catch(err){
                    console.log(err,"4");
                    return resolve(returnData);
                }
            }).catch(err=>{
                console.log(err,"5");
                return resolve(returnData);
            })

       })

    }
}


function getApprovalSchema(approvalID){
    return new Promise((resolve,reject)=>{
        Schema.findById(approvalID).then(val=>{
            if(val){
                resolve(val);
            }else{
                reject()
            }
        }).catch(err=>{
            reject();
        })
    })
}

function updateApprovalRequest(approval_request_id,status){
    return new Promise(function(resolve,reject){
        Request.findById(approval_request_id,function(err,res){
            var nDate = moment().tz('Asia/Jakarta'); 
            res.updated_at=nDate.format();
            res.status=status;

            res.save(function(err){
                if (err) {
                    console.log("error");
                    resolve("error");
                };
                resolve("success");
            })
        })
    })
}



function getUserApprover(approvalID){
    return new Promise((resolve,reject)=>{
        UserSchema.find({approval:approvalID}).sort({level:1}).then(val=>{
            if(val.length>0){
                resolve(val);
            }else{
                reject()
            }
        }).catch(err=>{
            reject();
        })
    })   
}

function getTask(task_id){
    return new Promise((resolve,reject)=>{
        Task.findOne({_id:task_id,status_task:'request'}).populate('approval_request').then(val=>{
            if(val){
                resolve(val);
            }else{
                reject()
            }
        }).catch(err=>{
          
            reject();
        })
    })  
}

