const mongoose = require('mongoose');
const ContactGroup=mongoose.model('BroadcastGroup');
const ContactList=mongoose.model('BroadcastContactList');
const BroadcastCall=mongoose.model('BroadcastCall');

const Json2csvParser=require('json2csv').Parser;

// Load the full build.
var _ = require('lodash');
//using hook schema for setting delete children if the parent have deleted
//
const Json2csvTransform=require('json2csv').Transform;


const uuid=require('uuid');
var moment = require('moment-timezone');
const {Pool, Client} =require('pg'); 

let hangup_cause=require('../../model/HangupStatus');

//this for function per Module
let Office=require('../office/office');
let User=require('../users/user-info');
let ERROR_CODE={
    UNDEFINED_VARIABLE:"UNDEFINED_VARIABLE",
    DB_ERROR:"CANNOT FIND DATA"
}
//get office 
exports.getOfficeBranch=async function(req,res,next){
    let userID=req.params.userID;



    


    //{ status:"false",errors:{},data:returnData}
    //get user info
    if(userID==null || userID=='undefined' || userID==''){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.UNDEFINED_VARIABLE,
                message:"user id is needed"
            },data:{}
        });   
    }

    redisClient.get(`officebranchReport:${userID}`,(err,result)=>{
        if(result){
    
            const results=JSON.parse(result);
            return res.json({
                status:true,
                errors:{},
                data:results
            });
        }else{
            User.userDetail(userID).then((val)=>{
     
                if(val.status){
                    let officeParent=val.data.office._id;
                    Office.officeByParents(officeParent).then((val)=>{
                        if(!val.status){
                            throw Error("cannot find data");
                        }
                        redisClient.setex(`officebranchReport:${userID}`,300,JSON.stringify(val.data));

                        return res.json({
                            status:true,
                            errors:{},
                            data:val.data
                        });
                    }).catch(err=>{
                        return res.json({
                            status:false,
                            errors:{
                                code:ERROR_CODE.DB_ERROR,
                                message:err
                            },data:{}
                        });
                    });
                    
                }else{
                    throw Error("cannot find data");
                }
        
            }).catch((err)=>{
                return res.json({
                    status:false,
                    errors:{
                        code:ERROR_CODE.DB_ERROR,
                        message:err
                    },data:{}
                });
            })
            
        }
    })
  
    
}

//get user all user by office
exports.getBroadcastByAllUser=async function(req,res,next){
    let officeID=req.body.data.office_id;
    let fromDate=moment(req.body.data.from_date).toDate();
    let toDate=moment(req.body.data.to_date).toDate();


    if(officeID==null || officeID=='undefined' || officeID==''){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.UNDEFINED_VARIABLE,
                message:"office id is needed"
            },data:{}
        });   
    }

    if(fromDate=='' || toDate=='' || fromDate==null || toDate==null){
        return res.json({
            status:false,
            errors:{
                code:ERROR_CODE.UNDEFINED_VARIABLE,
                message:"from date and to date are needed"
            },data:{}
        });  
    }

    
    //user data format
    // {
    //     user_name:"",
    //     user_id:"",
    //     total_contacting_call:"",
    //     success_call:"",
    //     others:""
    // }

  
    redisClient.get(`broadcastPerUserBranchOffice:${officeID},${fromDate},${toDate}`,(err,result)=>{
        if(result){
            const results=JSON.parse(result);
            return res.json({
                status:true,
                errors:{},
                data:results
            });
        }else{
            User.userByOffice(officeID).then(async (val)=>{
                let newUserData=[];
                if(val.status){
                    //get all broadcast per one user and get data all contact call
                    await Promise.all(val.data.map(async (dt)=>{
                        
                        let userData={
                            user_name:"",
                            user_id:"",
                            total_contacting_call:0,
                            answer:0,
                            ring_not_answer:0,
                            others:0,
                            total_answer_call_duration:""
                        }
                        userData.user_name=dt.email;
                        userData.user_id=dt._id;
        
                
                        let accountCode=[];
                            let userID=String(dt._id);
                      
                            await BroadcastCall.aggregate([
                                {$match:{
                                    user_id:userID,
                                    created_at:{$gte:fromDate,$lt:toDate},
                                    call_status:'Finish'
                                }},{$group:{
                                    _id:null,
                                    count:{$sum:1},
                                    total_contacting_call:{$sum:"$contact_count"},
                                    accountcode:{
                                        $addToSet:"$_id"
                                    }
                                }}]).then(async (val)=>{
                             
                                    
                                    if(val.length>0){
                                                                    //get data contact
                                        userData.total_contacting_call=val[0].total_contacting_call;
                                        //get success call from DB ring not answer, answer
                                        let totalContact=userData.total_contacting_call;
                    
                                        const pool = new Pool();
                                        const client=await pool.connect();
                                        //get data from db
                                        //change data from array to string;
                                        let newAccountCode=val[0].accountcode.map((dt)=>{
                                        return  "'"+dt+"'";
                                        })
                                
                                        let statusAnswer=hangup_cause.ANSWER.map((dt)=>{
                                        return "'"+dt+"'";
                                        })
                                
                                        let statusNoAnswer=hangup_cause.RING_NOT_ANSWER.map((dt)=>{
                                        return "'"+dt+"'";
                                        })
                    
                                        let query="";
                                        query+="SELECT COUNT(*), SUM(billmsec) FROM v_xml_cdr";
                                        query+=" where accountcode in ("+newAccountCode+")";
                                        //query+=" and hangup_cause in ("+statusAnswer+")";
                                        query+=" and billmsec!=0";
                                        const dataAnswer=await client.query(query);
                                        let answer=dataAnswer.rows[0].count;
                                        console.log(dataAnswer.rows[0]);
                                        let call_duration=dataAnswer.rows[0].sum;
                    
                                        //ring not answer
                                        let newQuery="";
                                        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
                                        newQuery+=" where accountcode in ("+newAccountCode+")";
                                        newQuery+=" and hangup_cause in ("+statusNoAnswer+")";
                                        newQuery+=" and billmsec=0"
                                        const dataNoAnswer=await client.query(newQuery);
                                        let noAnswer=dataNoAnswer.rows[0].count;
                    
                                        let totalAnswerNoAnswer=parseInt(answer)+parseInt(noAnswer);
                                        let others=totalContact-totalAnswerNoAnswer;
                    
                                        userData.answer=answer;
                                        userData.ring_not_answer=noAnswer;
                                        userData.others=others;
                                        userData.total_answer_call_duration=msToTime(call_duration);
                                
                                        await client.release();
                                        await pool.end();
                                    }
        
                                    newUserData.push(userData);
                                    userData={};
        
                            }).catch(err=>{
                               console.log(err);
                            })
        
        
                    }))
                }else{
                    throw Error("cannot Find User");
                }
                redisClient.setex(`broadcastPerUserBranchOffice:${officeID},${fromDate},${toDate}`,300,JSON.stringify(newUserData));
        
                return res.json({
                    status:true,
                    errors:{},
                    data:newUserData
                });
        
        
            }).catch(err=>{
                return res.json({
                    status:false,
                    errors:{
                        code:ERROR_CODE.DB_ERROR,
                        message:err
                    },data:{}
                });
            })
        }
    });

}

exports.downloadBroadcastByAllUser=async function(req,res,next){
    let officeID=req.body.data.office_id;
    let fromDate=moment(req.body.data.from_date).toDate();
    let toDate=moment(req.body.data.to_date).toDate();

    if(officeID==null || officeID=='undefined' || officeID==''){
        res.setHeader('Access-Control-Expose-Headers','Content-disposition');
        res.setHeader('Content-disposition', 'attachment; filename=empty.csv');
        res.set('Content-Type', 'text/csv');
        res.type('blob');
    
        //field
        const fields=['user_name','total_contacting_call','answer','ring_not_answer','others','total_answer_call_duration'];
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse([]);
        return res.send(csv);
    }

    if(fromDate=='' || toDate=='' || fromDate==null || toDate==null){
        res.setHeader('Access-Control-Expose-Headers','Content-disposition');
        res.setHeader('Content-disposition', 'attachment; filename=empty.csv');
        res.set('Content-Type', 'text/csv');
        res.type('blob');
    
        //field
        const fields=['user_name','total_contacting_call','answer','ring_not_answer','others','total_answer_call_duration'];
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse([]);
        return res.send(csv);
    }


  

    
    let officeName;
    
    try{
        let office;
        office=await Office.officeDetail(officeID);
        if(office.status){
            officeName=office.data.name;
        }else{
            officeName='unknown';
        }
        
    }catch(err){
        officeName='unknow';
    }

    let nameOfFile;
    nameOfFile='Call_Broadcast_Per_User_'+officeName+'_'+req.body.data.tanggal_name.replace(/[ ,.]/g,'_')+'.csv';

    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename='+nameOfFile);
    res.set('Content-Type', 'text/csv');
    res.type('blob');

    //field
    const fields=['user_name','total_contacting_call','answer','ring_not_answer','others','total_answer_call_duration'];
    const json2csvParser = new Json2csvParser({ fields });


    
    //user data format
    // {
    //     user_name:"",
    //     user_id:"",
    //     total_contacting_call:"",
    //     success_call:"",
    //     others:""
    // }

  
    redisClient.get(`broadcastPerUserBranchOffice:${officeID},${fromDate},${toDate}`,(err,result)=>{
        if(result){
            const results=JSON.parse(result);
            const csv = json2csvParser.parse(results);
            return res.send(csv);
        }else{
            User.userByOffice(officeID).then(async (val)=>{
                let newUserData=[];
                if(val.status){
                    //get all broadcast per one user and get data all contact call
                    await Promise.all(val.data.map(async (dt)=>{
                        
                        let userData={
                            user_name:"",
                            user_id:"",
                            total_contacting_call:0,
                            answer:0,
                            ring_not_answer:0,
                            others:0,
                            total_answer_call_duration:""
                        }
                        userData.user_name=dt.email;
                        userData.user_id=dt._id;
        
                
                        let accountCode=[];
                            let userID=String(dt._id);
                      
                            await BroadcastCall.aggregate([
                                {$match:{
                                    user_id:userID,
                                    created_at:{$gte:fromDate,$lt:toDate},
                                    call_status:'Finish'
                                }},{$group:{
                                    _id:null,
                                    count:{$sum:1},
                                    total_contacting_call:{$sum:"$contact_count"},
                                    accountcode:{
                                        $addToSet:"$_id"
                                    }
                                }}]).then(async (val)=>{
                             
                                    
                                    if(val.length>0){
                                                                    //get data contact
                                        userData.total_contacting_call=val[0].total_contacting_call;
                                        //get success call from DB ring not answer, answer
                                        let totalContact=userData.total_contacting_call;
                    
                                        const pool = new Pool();
                                        const client=await pool.connect();
                                        //get data from db
                                        //change data from array to string;
                                        let newAccountCode=val[0].accountcode.map((dt)=>{
                                        return  "'"+dt+"'";
                                        })
                                
                                        let statusAnswer=hangup_cause.ANSWER.map((dt)=>{
                                        return "'"+dt+"'";
                                        })
                                
                                        let statusNoAnswer=hangup_cause.RING_NOT_ANSWER.map((dt)=>{
                                        return "'"+dt+"'";
                                        })
                    
                                        let query="";
                                        query+="SELECT COUNT(*),SUM(billmsec) FROM v_xml_cdr";
                                        query+=" where accountcode in ("+newAccountCode+")";
                                        //query+=" and hangup_cause in ("+statusAnswer+")";
                                        query+=" and billmsec!=0";
                                        const dataAnswer=await client.query(query);
                                        let answer=dataAnswer.rows[0].count;
                                        let call_duration=dataAnswer.rows[0].sum;
                                        console.log(dataAnswer.rows[0]);
                                        //ring not answer
                                        let newQuery="";
                                        newQuery+="SELECT COUNT(*) FROM v_xml_cdr";
                                        newQuery+=" where accountcode in ("+newAccountCode+")";
                                        newQuery+=" and hangup_cause in ("+statusNoAnswer+")";
                                        newQuery+=" and billmsec=0";
                                        const dataNoAnswer=await client.query(newQuery);
                                        let noAnswer=dataNoAnswer.rows[0].count;
                    
                                        let totalAnswerNoAnswer=parseInt(answer)+parseInt(noAnswer);
                                        let others=totalContact-totalAnswerNoAnswer;
                    
                                        userData.answer=answer;
                                        userData.ring_not_answer=noAnswer;
                                        userData.others=others;
                                        userData.total_answer_call_duration=msToTime(call_duration);
                                
                                        await client.release();
                                        await pool.end();
                                    }
        
                                    newUserData.push(userData);
                                    userData={};
        
                            }).catch(err=>{
                               console.log(err);
                            })
        
        
                    }))
                }else{
                    throw Error("cannot Find User");
                }
                redisClient.setex(`broadcastPerUserBranchOffice:${officeID},${fromDate},${toDate}`,300,JSON.stringify(newUserData));

                const csv = json2csvParser.parse(newUserData);
                return res.send(csv);

        
        
            }).catch(err=>{
                const csv = json2csvParser.parse([]);
                return res.send(csv);
            })
        }
    });

}



//get all report survey

exports.getReportSurvey=async function(req,res,next){
    let userID=req.body.user_id;
    let ivr_menu_uid=req.body.ivr_menu_uid;

    let fromDate=moment(req.body.from_date).toDate();
    let toDate=moment(req.body.to_date).toDate();
    console.log(userID);

    let getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.find({user_id:String(userID),
            application:String(ivr_menu_uid),
            created_at:{$gte:fromDate,$lt:toDate},
            call_status:'Finish'}).then((val)=>{
                
            resolve(val);
        }).catch(err=>{
            reject(err);
        })
    })

    let dataResponse={
        data_survey:"",
        total_contact:"",
        account_code:""
    }

    try{
        let broadcastData=await getBroadcastCall;

        if(broadcastData.length>0){
            let collectAccountCode=await broadcastData.map(val=>{
                return "'"+val._id+"'";
            })
            let collectAccountCode1=await broadcastData.map(val=>{
                return val._id;
            })
      
            
            let dataSurvey=await getSurveyReport(ivr_menu_uid,collectAccountCode);

            var totalCall = broadcastData.reduce(function (accumulator, dt) {
                return accumulator + dt.contact_count;
              }, 0);
            //query database pgsql

            dataResponse.total_contact=totalCall;
            dataResponse.data_survey=dataSurvey;
            dataResponse.account_code=collectAccountCode1;

        }
    }catch(err){
        console.log(err);
        return res.json({status:false, errors:{
            code:"ERR",
            message:"ERROR"
        },data:{}})
    }

    return res.json({status:true, errors:{},data:dataResponse})

    //get all call that have userID and ivr_menu uid same as request
    //fet
}

async function getSurveyReport(application,accountcode){
    const pool = new Pool();
    const client=await pool.connect();
    //data model to be send
    //get data from cdr where call hangup == no answer
    let dataPush=[];
    try{
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_survey_pivot";
        newQuery+=" where uuid=$1";
        newQuery+=" order by created_at asc";
 
        const ivrSurvey =await client.query(newQuery, [application]);

       // console.log(ivrSurvey.rows);
        await Promise.all(ivrSurvey.rows.map(async function(val){
            try{
    
               
                var newQuery="";
                newQuery+="SELECT * FROM v_ivr_menus";
                newQuery+=" where ivr_menu_uuid=$1";
                newQuery+=" and ivr_type='MULTI_CHOICE'";
                let ivrMenu=await client.query(newQuery, [val.ivr_menu_uuid]);
                


                if(ivrMenu.hasOwnProperty('rows')){
                    await Promise.all(ivrMenu.rows.map(async function(data){
                        let dataObject={menu_name:'',menu_id:'',survey:[]};
                        dataObject.menu_name=data.ivr_menu_description;
                        dataObject.menu_id=data.ivr_menu_uuid
                        var query="";
                        //join with 
                        query+="SELECT v_ivr_menu_options.*,coalesce(options.jml,0) as survey_count  FROM v_ivr_menu_options";
                        query+=" left join (select ivr_menu_option_uuid, count(*) as jml ";
                        query+="from v_ivr_survey_report where call_broadcast_uuid in ("+accountcode.toString()+") group by ivr_menu_option_uuid)";
                        query+=" options on CAST(options.ivr_menu_option_uuid as uuid)=v_ivr_menu_options.ivr_menu_option_uuid";
                        query+=" where v_ivr_menu_options.ivr_menu_uuid=$1";
                
                        try{
                            let ivrOptions=await client.query(query,[data.ivr_menu_uuid]);
                            if(ivrOptions.hasOwnProperty('rows')){
                                dataObject.survey=ivrOptions.rows;
                            }
                        }catch(err){
                            console.log("error here");
                            console.log(err);
                        }



                        dataPush.push(dataObject);
 
                    }))
                }

            }catch(err){
                return {succes:false,data:false};
                console.log(err);
            }
 
        }))

    }catch(err){
        return {succes:false,data:false};
        console.log(err);
        console.log("cannot fetch data call answers3");

    }
    await client.release();
    await pool.end();
    return {succes:true,data:dataPush};
}

exports.download=async function(req,res){

    
    let option_survey_id=req.body.option_survey_id;
    let userID=req.body.user_id;
    let ivr_menu_uid=req.body.ivr_menu_uid;
    let fromDate=moment(req.body.from_date).toDate();
    let toDate=moment(req.body.to_date).toDate();

    let tanggal_name=req.body.tanggal_name;

    const pool = new Pool();
    const client=await pool.connect();

    let userName;

    await User.userDetail(userID).then((val)=>{
       
        if(val.status){
            userName=val.data.email;
        }else{
            userName="unknown";
        }
        
    }).catch(err=>{
        console.log(err);
    })

    

    var getBroadcastCall=new Promise(function(resolve,reject){
        BroadcastCall.find({user_id:String(userID),
            application:String(ivr_menu_uid),
            created_at:{$gte:fromDate,$lt:toDate},
            call_status:'Finish'},function(err,res){
            resolve(res);
        })
    })
    let dataBroadcast;
    let accountcode;
    try{
      dataBroadcast =await getBroadcastCall;

      accountcode=dataBroadcast.map((dt)=>{
          return "'"+dt._id+"'";
      })
    }catch(err){
        console.log("cannot fetch broadcast call data");
    }

  

    //get report from v_ivr_survey_report by ivr_menu_option_uuid and call_broadcast_uuid;
    var newQuery="";
    newQuery+="SELECT * FROM v_ivr_survey_report";
    newQuery+=" where ivr_menu_option_uuid=$1 and call_broadcast_uuid in ("+accountcode.toString()+")";
    newQuery+=" order by created_at asc";
    let ivrSurvey;
    let ivrMenu;
    let data=[];
    const fields=[];
    try{
        ivrSurvey =await client.query(newQuery, [option_survey_id]);
        var newQuery="";
        newQuery+="SELECT * FROM v_ivr_menu_options";
        newQuery+=" where ivr_menu_option_uuid=$1";
        ivrMenu=await client.query(newQuery, [option_survey_id]);
        //let contactGroup=await ContactGroup.findById(dataBroadcast.phonebook).exec();

        ///close pg connection to avoid memory high usage in postgre db
        await client.release();
        await pool.end();


        fields.push('kolom1');
        fields.push('kolom2');
        fields.push('kolom3');
        fields.push('kolom4');
        fields.push('kolom5');
        fields.push('kolom6');
        fields.push('kolom7');
        fields.push('kolom8');
        fields.push('kolom9');

        await Promise.all(dataBroadcast.map(async(dBroadcast)=>{
      
            await Promise.all(ivrSurvey.rows.map(async (val)=>{
              
                try{
                    let destiNumber=phoneFormat(val.destination_number);
                   // console.log(destiNumber);
                    let contact=await ContactList.findOne({phone:destiNumber,contact_group:dBroadcast.phonebook}).exec();
                    if(contact){
                      
                        let dataParse=JSON.parse(JSON.stringify(contact.content_data));
                        let dataContact=await Object.keys(dataParse);
                        let dataDesc={};
                        let kolomCount=1;
                        await dataContact.map((dt)=>{
                            dataDesc['kolom'+kolomCount]=dataParse[dt];
                            kolomCount++;
                        });
                        
                        data.push(dataDesc);
                    }
                }catch(err){
                    console.log(err);
                    res.send({success:false,message:"error"});
                    console.log("cannot fetch contact list");
                }
            }))
        }))

    }catch(err){
        console.log(err);
        console.log("cannot find ivr survey")
        res.setHeader('Access-Control-Expose-Headers','Content-disposition');
        res.setHeader('Content-disposition', 'attachment; filename=empty.csv');
        res.set('Content-Type', 'text/csv');
        res.type('blob');
        fields.push('empty');
        const json2csvParser = new Json2csvParser({ fields });
        const csv = json2csvParser.parse([]);
        res.send(csv);
        
    }
    
    //get contact list by number phone contact group(get from broadcastCall);

    var getContactList="";
    

    // var cursor=Subscriber.find(query).exec(function(err,response){
    //     res.send(response);
    // });
    let nameOfFile;
    if(ivrMenu.rows.length>0){
        nameOfFile="("+userName+")"+ivrMenu.rows[0].ivr_menu_option_description.replace(/[ ,.]/g,'_')+'.csv';
    }else{
        nameOfFile='Result_Survey.csv';
    }
    
    

    res.setHeader('Access-Control-Expose-Headers','Content-disposition');
    res.setHeader('Content-disposition', 'attachment; filename='+nameOfFile);
    res.set('Content-Type', 'text/csv');
    res.type('blob');
    
    const json2csvParser = new Json2csvParser({ fields,header:false });
    const csv = json2csvParser.parse(data);


    res.send(csv);
}




var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}

function isEmpty(data){
    for(var key in data) {
        if(data.hasOwnProperty(key))
            return false;
    }
    return true;
  
}


function msToTime(duration) {
    var milliseconds = parseInt((duration % 1000) / 100),
      seconds = parseInt((duration / 1000) % 60),
      minutes = parseInt((duration / (1000 * 60)) % 60),
      hours = parseInt((duration / (1000 * 60 * 60)) % 24);
  
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
  
    return hours + ":" + minutes + ":" + seconds ;
  }
