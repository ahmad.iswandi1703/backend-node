
const mongoose = require('mongoose');
const ContactGroup=mongoose.model('WhatsappGroup');
const ContactList=mongoose.model('WhatsappContactList');
const Media=mongoose.model('WhatsappMedia');
const Broadcast=mongoose.model('WhatsappBroadcast');
const Subscriber=mongoose.model('WhatsappSubscriber');
const job=require('../job/whatsapp-upload');

const fs=require('fs');

exports.uploadContact=function(data){
    //data represent structure
    // {
    //     contact_group_name:
    //     user_id:localStorage.getItem('user_id'),
    //     contact_group_name:'',
    //     file_name:'',
    //     content_desc:[
    //       'Nomor',
    //       'Nama'
    //     ]
    // }
 
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
      });
    let group =new ContactGroup();
    group.contact_group_name=data.contact_group_name;
    group.contact_group_file=data.file_name;
    group.content_desc=data.content_desc;
    group.created_at=DateNow;
    group.updated_at=DateNow;
    group.user_id=data.user_id;
    //run job from here
 
    group.save(function(err){
        if (err) throw err;
        whatsapGroupId=group._id;
        job.running(data,whatsapGroupId);
    });

}


//this method will execute by job
exports.saveContactList=function(data){

    // {
    //     phone:String,
    //     name:String,
    //     messsageError:String,
    //     status:String, //not whatsapp number, or anything
    //     enabled:Boolean,
    // }
    let contact=new ContactList();
    contact.phone=data.phone;
    contact.name=data.name;
    contact.messageError=data.messageError;
    contact.status=data.status;
    contact.enabled=data.enabled;
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
      });
    contact.created_at=DateNow;
    contact.updated_at=DateNow;
    contact.contact_group=data.contact_group;
    contact.user_id=data.user_id;
    contact.content_data=data.content_data;
    
    contact.save(function(error){
        //send exception
        if(error) throw error;
        
    });

}

exports.getContactGroupAll=function(req,res){
    ContactGroup.find({user_id:req.body.user_id}).sort({created_at:-1}).exec(function(err,result){
        if (err) throw err;
        if(result.length>0){
            res.json({success:true,value:result});
           
        }else{
            res.json({success:false,value:[]});
        }

    })
}

exports.getMediaAll=function(req,res){
    Media.find({user_id:req.body.user_id}).sort({created_at:-1}).exec(function(err,result){
        if (err) throw err;
        if(result.length>0){
            res.json({success:true,value:result});
           
        }else{
            res.json({success:false,value:[]});
        }

    })
}

exports.getContactGroup=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { created_at: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit,
    };
   
    ContactGroup.paginate({user_id:req.body.user_id}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};


exports.getMedia=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { created_at: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit,
    };
   
    Media.paginate({user_id:req.body.user_id}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};

exports.getContactList=function(req,res){

    let options={
        sort:     { created_at: -1 },
        page:   req.body.offset, 
        lean:true,
        limit:    req.body.limit
    };
    let group_id=req.body.data.group_id;
    let status=req.body.data.status;

    var query={};
    if(group_id!="all"){
        query.contact_group=group_id;

    }


    ContactList.paginate(query,options).then(response=>{
    
        res.send(response);
    }).catch(function(){
        //doing nothing if any error;
    });
}


exports.addMedia=function(data){
    let DateNow=new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
      });
    let media =new Media();
    media.media_name=data.media_name;
    media.file_name=data.file_name;
    media.created_at=DateNow;
    media.updated_at=DateNow;
    media.user_id=data.user_id;
    //run job from here
 
    media.save(function(err){
        if (err) throw err;
        return true;
    });
}

exports.deleteMedia=function(req,res){
    let id=req.body.uid;

    console.log(id);

    Media.findByIdAndRemove(id,function(err,media){
        if (err)
            throw err;
        fs.unlinkSync("dist/media/" + media.file_name, function () { //This Deletes The Temporary File
            //Moving File Completed
            console.log("work");
        });

        res.send({ success: true, message: "Deleted" });
    });
}

exports.broadcast=function(req,res){
    let broadcast_name=req.body.broadcast_name;
   
    let user_id=req.body.user_id;
    var nDate = new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Jakarta'
      });

    let content_data={
        contact_group:req.body.contact_group,
        message_type:req.body.message_type,
        media_url:req.body.media_url,
        media_description:req.body.media_description,
        text:req.body.text
    }

    let BroadcastWhatsapp=new Broadcast({
        broadcast_name:broadcast_name,
        broadcast_status:"starting",
        broadcast_date:nDate,
        broadcast_subscriber:0,
        created_at:nDate,
        updated_at:nDate,
        content_data:content_data,
        user_id:user_id
    })

    BroadcastWhatsapp.save(function(err){
        if (err) res.json({success:false,value:'error when insert'});
        
        let broadcastId=BroadcastWhatsapp._id;
        let data=content_data;
        //run job 
        let job=require('../job/whatsapp-broadcast');
        job.running(data,broadcastId).catch(error=>{
            console.log(error);  
            if(error) throw error;
        });
        res.json({success:true,value:'insert success'});
    })

}

exports.getWhatsappBroadcast=function(req,res){
    //-----Post request
    // limit:10,
    // offset:1,
    // sort:{
    //   field:"broadcast_date",
    //   sort:"desc"
    // }
    let options={
        sort:     { broadcast_date: -1 },
        lean:     true,
        page:   req.body.offset, 
        limit:    req.body.limit
    };

    var start = new Date()
    let hrstart=process.hrtime();
    console.log("before");
    console.log("Waiting For Response");
    Broadcast.paginate({user_id:req.body.user_id,'broadcast_status':{$ne:"delete"}}, options)
    .then(response => {
      /**
       * Response looks like:
       * {
       *   docs: [...] // array of Posts
       *   total: 42   // the total number of Posts
       *   limit: 10   // the number of Posts returned per page
       *   page: 2     // the current page of Posts returned
       *   pages: 5    // the total number of pages
       * }
      */
            console.log("after");
            var end = new Date() - start,
            hrend = process.hrtime(hrstart)
            console.info('Execution time: %dms', end)
            console.info('Execution time (hr): %ds %dms', hrend[0], hrend[1] / 1000000)
        res.send(response);
    })
    .catch(function(){
        //handle query error;
        // set loggin
    });
};

exports.getSmsSubscriber=function(req,res){
    //-----Post request
        //data :{broadcast_id:''}
        // limit:10,
        // offset:1,
        // sort:{
        //   field:"broadcast_date",
        //   sort:"desc"
        // }
        let options={
            sort:     { broadcast_date: -1 },
            page:   req.body.offset, 
            lean:true,
            limit:    req.body.limit
        };
    //    Subscriber.find({"broadcast":req.body.data.broadcast_id}).exec(function(err,response){
    //        res.send(response);
    //    })
        Subscriber.paginate({"broadcast":req.body.data.broadcast_id}, options)
        .then(response => {
          /**
           * Response looks like:
           * {
           *   docs: [...] // array of Posts
           *   total: 42   // the total number of Posts
           *   limit: 10   // the number of Posts returned per page
           *   page: 2     // the current page of Posts returned
           *   pages: 5    // the total number of pages
           * }
          */
            res.send(response);
        })
        .catch(function(){
            //handle query error;
            // set loggin
            
        });
    }

    exports.deleteWaBroadcast=function(req,res){
        let broadcast_id=req.params.broadcast_id;
    
        Broadcast.findById(broadcast_id).then((val)=>{
            if(val){
                val.broadcast_status="delete";
    
                val.save((err)=>{
                    if(err){
                        return res.json({
                            status:false,
                            errors:{
                                code:"DB ERROR",
                                message:"CANNOT DELETE DATA"
                            },data:{}
                        });  
    
                    }
                    return res.json({
                        status:true,
                        errors:{
    
                        },data:{}
                    });  
                });
            }
        }).catch(err=>{
            return res.json({
                status:false,
                errors:{
                    code:"DB ERROR",
                    message:"CANNOT FIND BROADCAST"
                },data:{}
            });  
        })
    };