var moment = require('moment-timezone');

//console.log(moment().tz('Asia/Jayapura').format());

let date=new Date();
//console.log(date);
let a=moment.utc(date,'YYYY-MM-DD HH:mm:ssZZ').format();
let amd=moment(date,'YYYY-MM-DD HH:mm','Asia/Jakarta').format();


// ///disini untuk mengambil datanya
// //expired date
// let ne=moment("2019-04-08T17:00:00.000+00:00").tz('Asia/Jakarta').toLocaleString();
// console.log(ne,"df");
// console.log(amd >= ne);

// console.log(moment("2019-04-08T15:09:09.000+00:00").tz('Asia/Jakarta').toDate(),"history");
// console.log(moment("2019-05-06T17:00:00.000+00:00").toDate(),"history");
// console.log(moment().subtract(2,'days').toDate())
// // let a=moment.tz('2018-01-27 10:30','YYYY-MM-DD HH:mm','Indian/Reunion').utc();
// // console.log(a);
// console.log(moment().tz('Asia/Makassar').format());
// console.log(moment().tz('Asia/Jakarta').format());
// console.log(moment().tz('Asia/Jayapura').format());

// let m=moment(new Date(2019,03,07)).format();

// let dNewNow=moment(m);
// let newNa=dNewNow.clone().tz('Asia/Jakarta');

// console.log(newNa.format(),"DFD");

// let requiredForm=[{name:"type",required:true},{name:"user_id",required:true},{name:"created_by",required:true},{name:"status",required:false}];

// let dataRequest={
//     type:"bla",
//     user_id:"a",
//     created_by:"sdf",
//     joko:"",
// }

// console.log(typeof dataRequest.joko);

// console.log(moment("2019-04-11T00:00:00+07:00",'YYYY-MM-DD HH:mm').tz('Asia/Jakarta').toLocaleString());

// checkParam(requiredForm,dataRequest);
// function checkParam(requiredForm, data){

//     let paramRejected=0;
//     let dataReturn={
//         passed:false,
//         dataReject:[]
//     }
//     if(requiredForm.length >0){
//         for(let i=0; i<requiredForm.length; i++){
//             let form=requiredForm[i];
//             if(data.hasOwnProperty(form.name)){
//                 if(data[form.name]=="" || data[form.name]==null || data[form.name]==undefined ){
//                     if(form.required){
//                         paramRejected++;
//                         let infoReject={
//                             property:"",
//                             error:"",
//                         };

//                         infoReject.property=form.name;
//                         infoReject.error="Property cannot be null";
//                         dataReturn.dataReject.push(infoReject);
//                     }
//                 }
//             }else{

//                 if(form.required){
//                     paramRejected++;
//                     paramRejected++;
//                     let infoReject={
//                         property:"",
//                         error:"",
//                     };
//                     infoReject.property=form.name;
//                     infoReject.error="Property required";
//                     dataReturn.dataReject.push(infoReject);
//                 }
//             }
//         }
//     }

//     if(paramRejected >0){
//         dataReturn.passed=false;
//     }else{
//         dataReturn.passed=true;
//     }

//     console.log(dataReturn);
    
//     return dataReturn;

// }
var getTimeNew=function(timeToStart,timezone){
    let hours=moment(timeToStart).format("HH:mm").toString();
    let parseDate=moment(timeToStart).format("DD.MM.YYYY").toString().trim().split('.');
    let parse=hours.trim().split(":");
    let dNewNow=moment().tz(timezone);
    dNewNow.set({'year': parseDate[2], 'month': parseDate[1]-1,'date':parseDate[0],h: parse[0], m:parse[1]})
    return dNewNow;
}
var determineScheduleTime=function(dateNow,numOfDday,weekDays,daily_start_time,timezone){
    //stop the job and set nextRun At to tommorrow at start the day
    let nextRunAt="";
    //it mean weeken or saturday
    let daysBetween=0;
    if(numOfDday == 6){
        console.log("here");
        //it means saturday  add to the next day
        let foundNextDay=false;
        let indexDay=0; 
        //console.log(weekDays[0].length);
        while(!foundNextDay){
            if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                //check if next day is the day that forbided in weekdays
                nextRunAt= weekDays[indexDay];
                foundNextDay=true;
            }

            indexDay++;
            
        }
        daysBetween=indexDay;
        //if there is no next day then stop the broadcast
    }else{
        let foundNextDay=false;
        let indexDay=(numOfDday)+1; 
        let sumDay=0;
        console.log("here");
        //console.log(weekDays[0].length);
        while(!foundNextDay){
            if(weekDays[indexDay].length!=0 && weekDays[indexDay]!=null){
                nextRunAt= weekDays[indexDay];
                foundNextDay=true;
            }
            if(indexDay==weekDays.length-1){
                indexDay=0;
            }else{
                indexDay++;
            }
            sumDay=sumDay+1;
        }
        daysBetween=sumDay;
        //if there is no next day then stop the broadcast
    }
    console.log(daysBetween);
    var dateNext=dateNow.add(daysBetween,'days').tz(timezone);
    var startDay=daily_start_time;//get from setting
    var split=startDay.split(":");
    //get date fr
    var newDate=dateNext.set({
        hours:split[0],
        minutes:split[1]
    }).format();
    console.log(newDate);

    return newDate;
}

function changeHoursOfDateNow(dateNow,time){
    var startDay=time;//get from setting
    var split=startDay.split(":");
    var newDate=dateNow.set({
        hours:split[0],
        minutes:split[1]
    }).format();

    return newDate;
}
var determineSchedule=function(setting,timeToStart,callSoon){
    var moment = require('moment-timezone');
    var nDate = moment().tz(setting.timezone); 
    var dateNow=moment(timeToStart).tz('Asia/Jakarta');
    var numOfDday=dateNow.day();//sunday=0, monday=1,......, saturday=6
    var nextRun=new Date(Date.now() + 2000);
    var weekDays=setting.weekdays;

    var timeEnded=changeHoursOfDateNow(dateNow,setting.daily_end_time);
    var timeStarted=changeHoursOfDateNow(dateNow,setting.daily_start_time)
    var timeNow;
    if(callSoon=='true'){
        console.log("call immediatelys");
        timeNow=moment(timeToStart).tz(setting.timezone).format();
        //console.log(moment(timeToStart).tz(setting.timezone).format("HH:mm"),"startwit timezon");
    }else{
        console.log("call Schedue");
        //make new date with hours and minute from schedule
      
        timeNow=getTimeNew(timeToStart,setting.timezone).format();
        //add here

        //timeNow=moment(timeToStart).tz('Asia/Jakarta').format();
        
    }
    
    console.log(moment(timeToStart).format("HH:mm"),"startwithout timezon");
    
    console.log(timeNow,"now");
    console.log(timeStarted,"started");
    console.log(timeEnded,"ended");
    console.log(setting.daily_start_time);
    console.log(setting.daily_end_time);
    console.log(dateNow.format());
    let newDateQ=timeNow;
    if(timeNow<=timeStarted){
        var newtimeStarted=moment(setting.daily_start_time,"HH:mm").tz(setting.timezone).format("HH:mm");
        
        let parse=setting.daily_start_time.trim().split(":");
        let dNow=new Date();
        let dNewNow=moment().tz(setting.timezone);
        dNewNow.set({h: parse[0], m:parse[1]})
        console.log(dNewNow.format(),"new form");
        console.log(dNow,"form javascript data");

        let newNa=dNewNow.format();
        console.log(newNa,"new time started");

        //df
        //cek apakah hari ini merupakan hari libur atau tidak, kalau iya maka alokasikan ke hari berikutnya;
        if(weekDays[numOfDday].length!=0 && weekDays[numOfDday]!=null){
            console.log("kurang dari daily start time dan hari ini tidak libur");
            if(dateNow.format()>=timeStarted){
                nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
                newDateQ=nextRun;
            }else{
                newDateQ= newNa;
            }
            
        }else{
        //     console.log("kurang dari daily start time dan hari libut");
            //jika hari ini sudah besar dari daily start time maka tambah ke hari berikutnya;
                nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
                newDateQ=nextRun;
            

        }
        
    }

    console.log(timeNow>=timeEnded);
    console.log(timeNow,timeEnded);
    //jika call immediately, cek apakah waktu sekarang sudah lewat dari daily end time atau belum daily start time
    if(timeNow>=timeEnded){
        console.log("lewat dari daily end time");
        
        //call determineScheduleTime
        nextRun=determineScheduleTime(dateNow,numOfDday,weekDays,setting.daily_start_time,setting.timezone);
        newDateQ=nextRun;
    }
    console.log(newDateQ);
    return newDateQ;

    //cek apakah time now kecil dari daily start time


    //kalau lebih kecil start schedule di daily start time


    // tambah penjagaan untuk memvalidasi tanggal ketika 
}





run();
async function run(){
    //get setting by broadcast.settin
    var setting={};


        setting={"_id":"5cb2dcc9b46c450bc03f89f3","weekdays":["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],"caller_id_name":"Telkom","caller_id_number":"--","dialing_timeout":30,"max_call_duration":1000,"concurent_limit":70,"schedule_minute_per":50,"retries_call":0,"retries_call_between":180,"daily_start_time":"7:00","status":"active","daily_end_time":"21:00","type":"all","user_id":"","updated_id":"","updated_at":"2019-04-14T07:09:18.000Z","created_at":"2019-04-14T07:09:18.000Z","gateway_id":"c7b66330-5c4c-4bcd-8ec7-b399af4dd9a8","timezone":"Asia/Jakarta","setting_name":"Call Setting ZONA WIT","__v":0}
        let dateNow=new Date().toLocaleString('en-US', {
            timeZone: setting.timezone
        });

        console.log("ada setting");

        var moment = require('moment-timezone');
        //var nDate = moment().tz('Asia/Jakarta'); 
        let schedule= moment().tz(setting.timezone).add(1,"seconds").format();
        let status="Starting";
        // if(broadcast.call_immediately=="false"){
        //     schedule=broadcast.call_schedule;
        //     console.log(broadcast.call_immediately);
        // }
        console.log(setting);
        let nexDate=new Date(2019,5-1,13,22,00);
        //  let nexDate=date.year+"-"+date.month+"-"+date.day+" "+time.hour+":"+time.minute;
         // nexDate=toLocalTime(nexDate);
        let dNewNow=moment(nexDate);
        let newNa=dNewNow.clone().tz(setting.timezone);
        let call_schedule=newNa.format();
        console.log(call_schedule,"jo");
        let newSchedule=determineSchedule(setting,call_schedule,'false');
        console.log(newSchedule);
}


// var dateNow=moment().tz('Asia/Jakarta');
// var numOfDday=dateNow.day();

// var weekdays=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
// var daily_start_time="7:00";
// var timezone='Asia/Jayapura';
// let hasil=determineScheduleTime(dateNow,numOfDday,weekdays,daily_start_time,timezone);
// console.log(hasil);

