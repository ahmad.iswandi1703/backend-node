const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastJobNotif= new Schema({
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastCall',
        require:true
    },
    status:String,//error,success
    code:String, // FAILED_BACKEND_REQUEST, ERROR_CALL_SETTING, CONTACT_NOT_FOUND
    message:String,
    created_at:Date,
    updated_at:Date
});

BroadcastJobNotif.plugin(paginate);
mongoose.model('BroadcastJobNotif',BroadcastJobNotif);