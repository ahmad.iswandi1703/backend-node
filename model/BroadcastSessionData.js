const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastSessionData= new Schema({
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastCall',
        require:true
    },
    callFinish:Number,
    nextRow:Number,
    status:String,
    created_at:Date,
    updated_at:Date
});

BroadcastSessionData.plugin(paginate);
module.exports=mongoose.model('BroadcastSessionData',BroadcastSessionData);