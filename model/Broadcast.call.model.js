const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let BroadcastCall= new Schema({
    uid:String,
    presence_id:String,
    campaign_name:String,
    account_code:String,
    application:String,
    phonebook:{
        type:Schema.Types.ObjectId,
        ref:'BroadcastGroup',
        require:false
    },
    call_setting:{
        type:Schema.Types.ObjectId,
        ref:'CallSetting',
        require:true
    },
    contact_count:Number,
    call_immediately:String,
    call_schedule:Date,
    call_status:String, //if call_status === one_app_call don't showing
    call_estimated_time:String,
    user_id:String,
    created_at:Date,
    updated_at:Date
});

BroadcastCall.plugin(paginate);
mongoose.model('BroadcastCall',BroadcastCall);