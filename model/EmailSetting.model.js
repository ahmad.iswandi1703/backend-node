const mongoose=require('mongoose');
const Schema=mongoose.Schema;
// host: 'smtp.gmail.com',
// port: 587,
// secure: false,
// requireTLS: true,
// auth: {
//     user: "ahmad.iswandi1703@gmail.com",
//     pass: "A_ku@#_(inta"
// }
// {
//     user: "ahmad.iswandi1703@gmail.com",
//     pass: "A_ku@#_(inta"
// }
let EmailSetting= new Schema({
    host: String,
    port: Number,
    secure: Boolean,
    requireTLS: Boolean,
    auth:{
        user:{ type: String, lowercase: true, trim: true },
        pass:{type:String}
    },
    status:String,
    type:String, /// all user (default) specific user if all user
    user_id:String,
    created_by:String,
    created_at:Date,
    updated_at:Date
});

mongoose.model('EmailSetting',EmailSetting);