const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let ApprovalUser=new Schema({
    approval:{
        type:Schema.Types.ObjectId,
        ref:'ApprovalSchema',
        require:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        require:true
    },
    level:Number,
    //task:[String], //can reject,approve,approve final, reject final
    created_at:Date,
    updated_at:Date,
    created_by:String,
})

ApprovalUser.plugin(paginate);
mongoose.model('ApprovalUser',ApprovalUser);