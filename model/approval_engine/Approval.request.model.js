const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let ApprovalRequest=new Schema({
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        require:true
    },
    approval:{
        type:Schema.Types.ObjectId,
        ref:'ApprovalSchema',
        require:true
    },
    comment:String,
    status:String, //proccessing, approved, rejected
    approval_state:String,
    created_at:Date,
    updated_at:Date,
})

ApprovalRequest.plugin(paginate);
mongoose.model('ApprovalRequest',ApprovalRequest);