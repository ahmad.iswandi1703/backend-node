const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let ApprovalTask=new Schema({
    approval_request:{
        type:Schema.Types.ObjectId,
        ref:'ApprovalRequest',
        required:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        require:true
    },
    status_task:String, //approved, rejected
    created_at:Date,
    updated_at:Date,

})

ApprovalTask.plugin(paginate);
mongoose.model('ApprovalTask',ApprovalTask);