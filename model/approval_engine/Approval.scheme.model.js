const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');


let ApprovalScheme=new Schema({
    schema_name:String,
    schema_description:String,
    approval_level:Number,
    created_at:Date,
    updated_at:Date,
    created_by:String,
})

ApprovalScheme.plugin(paginate);
mongoose.model('ApprovalSchema',ApprovalScheme);