const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');


let CallSettingSubscriber=new Schema({
    user: {
        type:Schema.Types.ObjectId,
        ref:'User',
        require:false
    },
    setting:{
        type:Schema.Types.ObjectId,
        ref:'CallSetting',
        require:true
    },
    created_at:Date,
    updated_at:Date,
})
CallSettingSubscriber.plugin(paginate);

mongoose.model('CallSettingSubscriber',CallSettingSubscriber);