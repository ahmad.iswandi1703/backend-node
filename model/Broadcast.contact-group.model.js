const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const paginate=require('mongoose-paginate');

let broadcastContactGroupSchema= new Schema({
   
    contact_group_name:String,
    contact_group_file:String,
    content_desc:[String],
    status:String,
    created_at:Date,
    updated_at:Date,
    user_id:String
});

broadcastContactGroupSchema.plugin(paginate);

mongoose.model('BroadcastGroup',broadcastContactGroupSchema);