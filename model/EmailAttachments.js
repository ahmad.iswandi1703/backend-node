const mongoose=require('mongoose');
const Schema=mongoose.Schema;
var paginate=require('mongoose-paginate');

let attachmentSchema= new Schema({
    filename:[String],
    created_at:Date,
    updated_at:Date,
    broadcast:{
        type:Schema.Types.ObjectId,
        ref:'EmailBroadcast',
        required:true
    },
    user:{
        type:Schema.Types.ObjectId,
        ref:'User',
        require:true
    }
});
attachmentSchema.plugin(paginate);
mongoose.model('EmailAttachment',attachmentSchema);

//change model