const Agenda = require('agenda');
const { MongoClient } = require('mongodb');

var fs=require('fs');
var parse=require('csv-parse');
var twilio = require('twilio');
var client = new twilio(process.env.TWILIO_ACCOUNT_SID, process.env.TWILIO_AUTH_TOKEN);

var mongoose=require('mongoose');
const moment=require('moment');
const mTimezone=require('moment-timezone');
const BroadcastCtrl=require('../controllers/broadcast-controller');
var csv=require('csvtojson');
var parseData=require('../lib/parse-number');

async function run(data,broadcastContactGroupId){

    // console.log(data);
    // console.log(broadcastid);
    
    let jobName="a"+broadcastContactGroupId;
    var colDesc=data.content_desc;
    const filePath=data.file_name;

    var user_id=data.user_id;

    //const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
    const mongoConnectionString=process.env.MONGODB_URI;
    const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobque'}});

    // agenda.define(jobName, async function(job,done){
    //     //console.log("hello wirld"+job.attrs.data.broadcast_id);
    //     let broadcastId=job.attrs.data.broadcastContactGroupId;
    //     //done if all message send
    //     var sumRow=0;
    //     const converter=csv({
    //         noheader:true,
    //         trim:true,
    //         delimiter:";",
    //         quote:"off"
    //     })
       
    //     converter.on("data",(async (jsonObj)=>{
    //         const jsonStr= jsonObj.toString('utf8')
    //         const json=JSON.parse(jsonStr);
    //         let contact={};
    //         try{
    //             parseData(json,"field",colDesc,(err,returnData)=>{
    //                 if(err){
    //                     throw new Error(err);
    //                 }
    //                 //saving data to database
    //                 contact.phone=returnData.phone;
    //                 contact.name=returnData.name;
    //                 contact.messageError=returnData.message_error;
    //                 contact.status="check";
    //                 contact.enabled=returnData.enabled;    
    //                 contact.content_data=returnData.content_data;
    //                 contact.contact_group=broadcastContactGroupId;
    //                 contact.user_id=user_id;
    //             })
    //         }catch(err){
    //             console.log(err);
    //         }

    //         // //save data to Database;
    //         try{
    //             BroadcastCtrl.saveContactList(contact);
    //         }catch{
    //             console.log("error when saving contact list to database");
    //         }
    //     }));
    //     // var parseCsvFile=parse({delimiter:';'}, function(err,data){
    //     //     sumRow=data.length;
       
    //     //     data.forEach(el => {
    //     //         let contact={};
    //     //         var phone=el[0].trim();
    //     //         var name=el[1];
    //     //         var enabled=false;
    //     //         var messageError="valid";
    //     //         var status='check';
    //     //         var content_data={};
                
    //     //         //add guard to check if phone number is in the right format, contains digit 0-9
    //     //         if(matchPhoneFormat(phone)){
    //     //             phone=phoneFormat(phone);
                
    //     //             if(el.length!=colDesc.length){
    //     //                 enabled=false;
    //     //                 messageError="content description is not the same";
    //     //             }else{
    //     //                 $col=0;
    //     //                 enabled=true;
                        
    //     //                 colDesc.forEach(dt=>{
    //     //                     content_data[dt]=el[$col];
    //     //                     $col++;
    //     //                 })
    //     //             }
    //     //         }else{
    //     //             enabled=false;
    //     //             messageError="phone number is not valid";
    //     //         }

    //     //         contact.phone=phone;
    //     //         contact.name=name;
    //     //         contact.messageError=messageError;
    //     //         contact.status=status;
    //     //         contact.enabled=enabled;    
    //     //         contact.content_data=content_data;
    //     //         contact.contact_group=broadcastContactGroupId;
    //     //         contact.user_id=user_id;
                
    //     //         try{
    //     //             BroadcastCtrl.saveContactList(contact);
    //     //         }catch{
    //     //             console.log("error input contact list");
    //     //             //loging to database;
    //     //         }
                
    //     //     });

    //     // });
    //     await  fs.createReadStream(filePath,{ encoding: 'utf8',highWaterMark: 1024 }).pipe(converter);

    //     await fs.unlinkSync(filePath);
    //     //update 
    //     done();

    // })

    await new Promise(resolve => agenda.once('ready', resolve));

    agenda.schedule(new Date(Date.now() + 1000), "upload_contact_call",{
        broadcastContactGroupId:broadcastContactGroupId,
        data:{
            colDesc:colDesc,
            filePath:filePath,
            user_id:user_id,
            broadcastContactGroupId:broadcastContactGroupId
        }
    });
   
}

var matchPhoneFormat=function(number){
    let match= number.match(/^[0-9]*$/g);
    return match!=null?true:false;
}
var phoneFormat=function(n){

    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}



exports.running=run;