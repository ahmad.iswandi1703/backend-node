const Agenda = require('agenda');
//const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true";
const mongoConnectionString=process.env.MONGODB_URI
const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});
const mongoose = require('mongoose');
const jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];

const BroadcastCtrl=require('../controllers/broadcast-email-controller');

const Call=mongoose.model('EmailBroadcast');

var csv=require('csvtojson');
var parseData=require('../lib/parse-email').parseData;
var bodyParse=require('../lib/parse-email').bodyParse;
var email=require('../lib/send-email');
var fs=require('fs');

agenda.define("send-email-scheduler", async function(job,done){
    //console.log("hello wirld"+job.attrs.data.broadcast_id);
    let broadcastId=job.attrs.data.data._id;
    let colDesc=job.attrs.data.data.contet_desc;
    let filePath=job.attrs.data.filePath;
    let user_id=job.attrs.data.data.user_id;
    let content_message=job.attrs.data.data.content_message;
    let content_from=job.attrs.data.data.content_from;
    let content_subject=job.attrs.data.data.content_subject;
    var updateCallStatus=function(broadcast,status){
        return new Promise(function(resolve,reject){
            Call.findById(broadcast,function(err,res){
                var moment = require('moment-timezone');
                var nDate = moment().tz('Asia/Jakarta'); 
                res.updated_at=nDate.format();
                res.broadcast_status=status;
                res.save(function(err){
                    if (err) {
                        console.log("error");
                    };
                    resolve("success");
                })
            })
        })
    }

    try{
        let updateBroadcastCall= await updateCallStatus(broadcastId,"Processing");
        if(updateBroadcastCall=="success"){
            console.log("email "+additionalData._id+" is processing");
        }
    }catch(err){
        console.log(err);
    }

    //
    var sumRow=0;
    const converter=csv({
        noheader:true,
        trim:true,
        delimiter:";",
        quote:"off"
    });
    converter.on("data",(async (jsonObj)=>{
        const jsonStr= jsonObj.toString('utf8')
        const json=JSON.parse(jsonStr);
        let subscriber={};
        try{
            parseData(json,"field",colDesc,async (err,returnData)=>{
                if(err){
                    throw new Error(err);
                }
                //saving data to database

            
                //sending the email (generate message body)
                let messageBody=bodyParse(colDesc,content_message,returnData.content_data);
                subscriber.email=returnData.email,
                subscriber.name=returnData.name,
                subscriber.broadcast=broadcastId,
                subscriber.user_id=user_id;
                subscriber.messsage=messageBody.message;

                let subject=content_subject;
                let from=content_from;
                let to=returnData.email;
                let template=job.attrs.data.data.email_template;
                let setting=job.attrs.data.setting;
                //send email if success then add subscriber 

                if(messageBody.status!="failed"){
                    //call send email library, pass user id; get data return from callback function
                    await email.sendEmail(messageBody.message,from,to,subject,template,setting).then((val)=>{
                        //add to subscriber;
                        subscriber.status="delivered";
                    }).catch((err)=>{
                        console.log(err);
                        subscriber.status="failed";
                    })
                }else{
                    console.log(messageBody);
                    subscriber.status="failed";
                    //save data to subscriber but set status to failed;
                }
                await BroadcastCtrl.saveSubscriber(subscriber).then((val)=>{
                    console.log(val);
                }).catch(err=>{
                    console.log(err);
                })
            })
        }catch(err){
            console.log(err);
            //save 
        }
        // //save data to Database;
    }));

    await  fs.createReadStream(filePath,{ encoding: 'utf8',highWaterMark: 1024 }).pipe(converter);
    await fs.unlinkSync(filePath);
    console.log("finish");
    try{
        let updateBroadcastCall= await updateCallStatus(broadcastId,"Finish");
        if(updateBroadcastCall=="success"){
            console.log("email "+additionalData._id+" is Finish");
        }
    }catch(err){
        console.log(err);
    }
    //update 
    done();
})

agenda.on("ready", async function () {
    agenda.start();
})
module.exports = agenda;