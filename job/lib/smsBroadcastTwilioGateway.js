
var twilio = require('twilio');
module.exports=function(message,phone,broadcast_id,setting,callback){
    let url=setting.gateway_url || process.env.BAGIDATA_URL ;
    let token=setting.gateway_token || process.env.TWILIO_AUTH_TOKEN;
    let secret=setting.gateway_secret || process.env.TWILIO_ACCOUNT_SID;

    let account_service_sid=setting.gateway_specific_sid || process.env.TWILIO_MESSAGING_SERVICE_SID;

    var client = new twilio(secret, token);

    client.messages.create({
        body: message,
        to: phone,  // Text this number
        from: account_service_sid //'+17372487799' // From a valid Twilio number
    })
    .then(message => {
        let data={
            sid:message.sid,
            status:message.status,
            price:message.price,
            priceUnit:message.priceUnit
        }
    
        callback(null,data);
    },function(err){
        
        callback(err,null);
    })
    .done();

}
