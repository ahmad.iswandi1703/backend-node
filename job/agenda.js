const Agenda = require('agenda');
//const mongoConnectionString="mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/jobspecialize?retryWrites=true";
const mongoConnectionString=process.env.MONGODB_URI1;
//const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs'}});
const agenda =new Agenda({db: {address: mongoConnectionString, collection: 'jobs_scheduling_sms'}});

const jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [];
jobTypes.forEach(type => {
  require('./' + type)(agenda);
});

if (jobTypes.length) {
  agenda.start(); // Returns a promise, which should be handled appropriately
}

console.log(agenda);

module.exports = agenda;