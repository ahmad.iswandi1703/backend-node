// Example code contributed by Fraser Redmond
// WARNING: this code still uses the old (0.3.x) version of the module.
// TODO: Migrate to the new (2.0) version.

var fsServer = '178.128.215.77',
    fsPass   = 'clueCon',
    fsPort   = 8021;

var fsEsl = require('esl'),
    io    = require('socket.io')();
let client=fsEsl.reconnect({host:fsServer,port:fsPort},handler);
// client.connect(fsPort, fsServer);
function handler(){
   this.on('esl_auth_request', function(callbackObj) {
        return callbackObj.auth(fsPass, function(callbackObj) {
            console.log(callbackObj);
        });
    });
    this.event_json('BACKGROUND_JOB');

    this.event_json('CHANNEL_ANSWER'); // actually returns a Promise

    this.event_json('CHANNEL_HANGUP');
    this.on('CHANNEL_ANSWER',function(o) {
      //console.log(o);
  
      var headers = o.headers;
      var body = o.body;
      var dtmf = o.body['DTMF-Digit']
      
    })
    this.on('BACKGROUND_JOB',function(o) {
        console.log("JOB UUUUIDD =============");
        console.log(o.body['Job-UUID']);
        console.log("JOB QUEU =======================");
    })

    //node js scheduler call, The Task:
    // Grab data from broadcast subscriber
    // this scheduler call should manage the call (See status of bridging call, recall the number )
    // this scheduler will do broadcast call with concurrent call according to call setting
    // this scheduler should determine to continue call or not;
    //the purpose is to monitoring call 

    //call 100 subscriber
    //monitoring that call
    //run scheduler every seconds;
    //if calls active is more than 100 and dont do next call
    //if call is less than 100 and hitung selisih nya dan grab data next call sebanyak selisih

    //-- check NO_USER_RESPONSE IN CHANNEL_HANGUP

    //to re call this user;
    //detect channel hangup (IF NO_USER_RESPONSE) then get data number and account code 
    this.on('CHANNEL_HANGUP',function(o) {
        console.log(o.body["Core-UUID"])
        console.log(o.body["Hangup-Cause"]);
        //channel hangup 
        console.log("++++++++++++++++++++++++++++++");
       
        console.log("+++++++++++++++++++++++++++++++");
        console.log(o.body["Channel-Name"]);
    })

    this.event_json('CHANNEL_CALLSTATE');
    this.on('CHANNEL_CALLSTATE',function(o){
        //console.log(o);
    })


    this.event_json('ADD_SCHEDULE');
    this.on('ADD_SCHEDULE',function(o){
        console.log("==========schedule------------------============");
        console.log(o);
    })

    this.event_json('DEL_SCHEDULE');
    this.on('DEL_SCHEDULE',function(o){
        console.log("==========delete schedule------------------============");
        console.log(o);
    })
    this.event_json('EXE_SCHEDULE');
    this.on('EXE_SCHEDULE',function(o){
        console.log("==========EXE schedule------------------============");
        console.log(o);
    })


}

function fsConnect (socket, cmd, callbackFunc, args) {
    fsConnClosed(socket);
    
    socket.fsClient = fsEsl.createClient();
    socket.fsClient.on('close', function(){fsConnClosed(socket)});

    socket.fsClient.on('esl_auth_request', function(callbackObj) {
        return callbackObj.auth(fsPass, function(callbackObj) {
            socket.fsConn = callbackObj;
            fsCommand(socket, cmd, callbackFunc, args);
        });
    });

    s

    return socket.fsClient.connect(fsPort, fsServer);
}

function fsConnClosed (socket) {
    if (socket.fsClient) {
        socket.fsClient.end();
    }
    socket.fsConn = false;
    socket.fsClient = false;
}

function fsCommand (socket, cmd, callbackFunc, args) {
    if (!socket.fsConn || !socket.fsClient) {
        fsConnect(socket, cmd, callbackFunc, args); //If connection doesn't already exist, then create it, and when it's created it will call this function with the same params again
    } else {
        socket.fsConn.api(cmd, function(callback) {
            //console.log('text:' + callback.body);
            if (callbackFunc) {
                console.log(cmd)
                callbackFunc(callback.body, socket, args);
            }
        });
    }
}

function parseStrToArray (textToParse, columnDelimiter, returnNameValPairs) {
    var headerLines, headers, line, name, nameValPair;
    headerLines = textToParse.split("\n");
    if (returnNameValPairs) {
        headers = {};
    } else {
        headers = [];
    }
    for (var i = 0; i < headerLines.length; i++) {
        if (returnNameValPairs) {
            nameValPair = headerLines[i].split(columnDelimiter, 2)
            headers[nameValPair[0]] = nameValPair[1];
        } else {
            headers[i] = headerLines[i].split(columnDelimiter);
        }
    }
    if (returnNameValPairs && headers['Reply-Text'] && headers['Reply-Text'][0] === '%') {
        for (name in headers) {
            headers[name] = querystring.unescape(headers[name]);
        }
    }
    return headers;
}





function parseUuids (responseStr, socket, messageObj) {
    var uuidArr = parseStrToArray(responseStr, ',', false);
    //more code here...
}


io.on('connection', function(socket) {
    console.log('connected')
    socket.fsClient = false,
    socket.fsConn   = false,


    socket.on('message', function(messageObj){
        fsCommand(socket, "show channels like " + messageObj.extnNum, parseUuids, messageObj);
    });

    socket.on('disconnect', function(){
        fsConnClosed(socket);
    });
});
io.listen(3000);

process.on('exit', function () {
    conn.closeSync();
});