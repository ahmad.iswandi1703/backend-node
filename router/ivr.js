var express=require('express');

var router=express.Router();


var IvrCtrl=require('../controllers/ivr-controller');
router.post('/ivr-survey',IvrCtrl.addIvr);

router.post('/ivr-survey-edit',IvrCtrl.ivrSurveyEdit);

router.get('/common/uuid',IvrCtrl.getUUID);

router.post('/ivr-survey/view',IvrCtrl.getIvr);

router.get('/ivr-survey/view-edit/:uid',IvrCtrl.getIvrEdit);


router.get('/ivr-subscriber/:uid',IvrCtrl.getSubscriber);
router.post('/add-subscriber',IvrCtrl.addSubscriber);
router.post('/edit-subscriber',IvrCtrl.editSubscriber);

router.get('/ivr-per-user/:uid',IvrCtrl.getIvrPerUser)
module.exports=router;