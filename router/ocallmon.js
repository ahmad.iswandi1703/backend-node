var express=require('express');

var router=express.Router();


var SettingCtrl=require('../controllers/onecall-controller');
router.post('/setting',SettingCtrl.createSetting);
router.get('/setting/:user_id/:status',SettingCtrl.getSetting);


router.post('/make-campaign',SettingCtrl.makeCampaign);

router.get('/lastcall/:id',SettingCtrl.getLastCall);
module.exports=router;