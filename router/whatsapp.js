var express=require('express');

var router=express.Router();


const whatsappCtrl=require('../controllers/whatsapp-controller');



//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);

router.post('/group',whatsappCtrl.getContactGroup);
router.post('/list-group',whatsappCtrl.getContactGroupAll);

router.post('/contact-list',whatsappCtrl.getContactList);

router.post('/media',whatsappCtrl.getMedia);

router.post('/media-delete',whatsappCtrl.deleteMedia);
router.post('/list-media',whatsappCtrl.getMediaAll);


router.post('/broadcast',whatsappCtrl.broadcast);
router.post('/broadcast-list',whatsappCtrl.getWhatsappBroadcast);

router.post('/subscriber',whatsappCtrl.getSmsSubscriber);

router.get('/delete-broadcast/:broadcast_id',whatsappCtrl.deleteWaBroadcast);

module.exports=router;