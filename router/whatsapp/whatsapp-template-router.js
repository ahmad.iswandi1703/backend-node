var express=require('express');

var router=express.Router();


var WaTmplCtrl=require('../../controllers/whatsapp/whatsapp-template-setting-controller');
//add template
router.post('/template',WaTmplCtrl.addSmsTemplate);
router.post('/all-template',WaTmplCtrl.queryPagingSmsTemplate);

router.get('/get-template/:template_id',WaTmplCtrl.getSmsTemplate);
router.post('/edit-template',WaTmplCtrl.editTemplate);
router.get('/delete-template/:template_id', WaTmplCtrl.deleteTemplate);
//edit
//delete
router.get('/subscriber-by-template/:sms_template_id',WaTmplCtrl.getSubscriberByTemplate);
router.post('/add-subscriber',WaTmplCtrl.addSubscriber);
router.post('/edit-subscriber',WaTmplCtrl.editSubscriber);
router.get('/template-per-user/:user_id',WaTmplCtrl.getTemplateBySubscriber);


module.exports=router;