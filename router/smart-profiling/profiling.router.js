var express=require('express');

var router=express.Router();
const profilling=require('../../controllers/profiling/profillingPropertyController');

router.post('/draft',profilling.addData);
router.get('/detail/:id',profilling.getDataDetail);

//filter data
router.get('/get-filter',profilling.getFilterData);

//get total prospect
router.post('/total-prospect', profilling.getTotalProspect);

//deleted data
router.get('/delete/:id',profilling.deleteData);


module.exports=router;