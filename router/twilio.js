var express=require('express');
var twilio=require('twilio');
var router=express.Router();


const twilioController=require('../controllers/twilio-controller');
const nexmoController=require('../controllers/nexmo/nexmo-controller');


//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);

// router.post('/callback', twilio.webhook,twilioController.endpoint);
router.post('/message-status',nexmoController.statusCallback);
router.post('/inbound',nexmoController.inBound);

module.exports=router;