var express=require('express');

var router=express.Router();


const smsController=require('../controllers/sms-controller');



//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);

router.post('/subscriber',smsController.getSmsSubscriber);


router.post('/broadcast',smsController.getSmsBroadcast);
router.get('/delete-broadcast/:broadcast_id',smsController.deleteSmsBroadcast);
module.exports=router;