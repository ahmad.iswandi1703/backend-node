var express=require('express');
var twilio=require('twilio');
var router=express.Router();
var auth=require('./auth');

const userCtrl=require('../controllers/user-management-controller');



//get sms broadcast data;
//router.get('/broadcast',smsController.getSmsBroadcast);

router.post('/',userCtrl.newUser);
router.post('/login',userCtrl.login);

//GET current route (required, only authenticated users have access)
// router.post('/current', auth.required, userCtrl.current);
router.post('/get-user', userCtrl.current);

router.post('/all-user',userCtrl.getAllUser);
router.post('/change-password',userCtrl.changePassword);
router.delete('/delete-user/:uid',userCtrl.deleteUser);
router.get('/all-user',userCtrl.getUser);
module.exports=router;