var express=require('express');

var router=express.Router();


var DashboardCtrl=require('../controllers/dashboard');
router.post('/success-rate',DashboardCtrl.getSuccessRate);
router.post('/survey-report',DashboardCtrl.getReportSurvey);
router.post('/survey-download',DashboardCtrl.download);
router.post('/all-call-report',DashboardCtrl.getCallReport);
router.post('/all-sms-report',DashboardCtrl.getSmsReport);

router.get('/office-branch/:officeID',DashboardCtrl.getOfficeBranch);

module.exports=router;