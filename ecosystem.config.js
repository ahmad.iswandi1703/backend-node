module.exports = {
  apps : [{
    name: "app",
    script: "./app.js",
    instances:"6",
    exec_mode:"cluster",
    env: {
      NODE_ENV: "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
}
// apps : [{
//   name: "app",
//   script: "./app.js",

//   env: {
//     NODE_ENV: "development",
//   },
//   env_production: {
//     NODE_ENV: "production",
//   }
// }]

  // apps : [{
  //   name: "app",
  //   script: "./app.js",
  //   instances:"max",
  //   exec_mode:"cluster",
  //   env: {
  //     NODE_ENV: "development",
  //   },
  //   env_production: {
  //     NODE_ENV: "production",
  //   }
  // },{
  //   name: "contact-upload-scheduler",
  //   script: "./scheduler-update-contact.js",
  //   env: {
  //     NODE_ENV: "development",
  //   },
  //   env_production: {
  //     NODE_ENV: "production",
  //   }
  // }]