const mongoose = require('mongoose');
let dev_db_url = 'mongodb+srv://ahmad_iswandi1703:RJjI61kXj6wzParc@cluster0-ao9zy.mongodb.net/test?retryWrites=true';
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(mongoDB,{ useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
const cors = require('cors');
var moment = require('moment-timezone');
var path = require('path');
global.appRoot = path.resolve(__dirname);
require('dotenv').config();

//initiate models
require('./model/user');

require('./model/Broadcast.email.model');
require('./model/Broadcast.email.subscriber.model');
require('./model/EmailSetting.model');


//initiate agenda.js
agenda=require('./job/agenda-send-email-scheduler');
async function graceful() {

    //cancel recall call
    await agenda.stop();
    process.exit(0);
}
process.on('SIGTERM', graceful);
process.on('SIGINT' , graceful);