var fs=require('fs');

var csv=require('csvtojson');
//create phone number
//match phone format
var parseData=function(row,nameFieldIdentifier,contentDesc,callback){

    let cDLength=0;
    
    let returnData={
        success:"",//failed or success parse row
        content_data:"",
        phone:"",
        name:"",
        enabled:"",
        message_error:""
    }
    if(Array.isArray(contentDesc)){
        cDLength=contentDesc.length;
    }
    
    let contentData={};
    for(let i=0; i<cDLength; i++){
        let field=nameFieldIdentifier+(i+1);
        if(row.hasOwnProperty(field)){
            contentData[contentDesc[i]]=row[field];
        }else{
            contentData[contentDesc[i]]="";
        }
    }

    if(typeof row != "object"){
        returnData.success=false;
        returnData.message_error="object row is not defined";
        callback(returnData.message_error,null);
    }

    let iField=nameFieldIdentifier;
    //konten Default
    let phone=row.hasOwnProperty(iField+"1")?row[iField+"1"].trim():"";
    let name=row.hasOwnProperty(iField+"2")?row[iField+"2"]:"";

    if(matchPhoneFormat(phone)){
        //check if number has prefix +62
        if(phone.match(/^[\+]62/g)==null){
            phone=phoneFormat(phone);
        }

        returnData.success=true;
        returnData.content_data=contentData;
        returnData.phone=phone;
        returnData.name=name;
        returnData.enabled=true;
        returnData.message_error="valid";
        
        callback(null,returnData);
    }else{
        //contact broken
        returnData.success=false;
        returnData.message_error="error format in phone number";
        callback(returnData.message_error,null);
    }

    

}

var matchPhoneFormat=function(number){
    let match= number.match(/^[\+]?[0-9]*$/g);
    return match!=null?true:false;
}

var phoneFormat=function(n){
    n=n.replace(/\([0-9]+?\)/,"");
    n=n.replace(/[^0-9]/,"");
    n=n.replace(/^0+/,"");
    defaultPrefix="+62";
    regex=new RegExp('^[\+]'+defaultPrefix,"g");
    var matchNum=n.match(regex);

    if (matchNum==null ) {
        n = defaultPrefix+n;
    }
    return n;
}

module.exports=parseData;