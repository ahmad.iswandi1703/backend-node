///get setting email by user
var EmailTemplate=require('email-templates');
var nodemailer = require('nodemailer');
var path = require("path");
var smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: "ahmad.iswandi1703@gmail.com",
        pass: "A_ku@#_(inta"
    }
});
//let ejs = require('ejs'),
//let ejs = require('ejs'),
let ejs = require('ejs');
// ejs.renderFile(templateDir, {name:"ahmad"}, function(err, str){
// // str => Rendered HTML string

//     const mailOptions = {
//     from: 'ahmad.iswandi1703@gmail.com', // sender address
//     to: 'ahmad.iswandi1703@gmail.com', // list of receivers
//     subject: 'Subject of your email', // Subject line
//     html: str// plain text body
//     };

//     transporter.sendMail(mailOptions, function (err, info) {
//     if(err)
//         console.log(err)
//     else
//         console.log(info);
//     });
// });

exports.sendEmail=function(message,from,to,subject,template,setting){
    var transporter = nodemailer.createTransport({
        host: setting.host,
        port: setting.port,
        secure: setting.secure,
        requireTLS: setting.secure,
        auth: {
            user: setting.auth.user,
            pass: setting.auth.pass
        }
    });
    let ejs = require('ejs');
    var templateDir = path.join(__dirname, 'email-templates', template,'html.ejs');

    return new Promise((resolve,reject)=>{
        ejs.renderFile(templateDir, {message:message}, function(err, str){
            // str => Rendered HTML string
                const mailOptions = {
                from: from, // sender address
                to: to, // list of receivers
                subject: subject, // Subject line
                html: str// plain text body
                };

                transporter.sendMail(mailOptions, function (err, info) {
                    if(err){
                        console.log(err)
                        reject(err);
                    }
                    console.log(info);
                    resolve(info);
                });
        });
    })
}